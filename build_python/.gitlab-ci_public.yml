---
# BBOSS Copyright (C) 2019, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# Project variable examples :
#  BUILD_SCT_PRODUCT:  'bboss'
#  BUILD_SCT_CMDARGS:  ''

# Schedule variables examples :
#  BUILD_SCT_CMDARGS:  '-vl'
#  BUILD_SCT_BUMPVER:  'build'
#  BUILD_SCT_PUSHREPO: 'pypi-hosted'

variables:
  BUILD_SCT_LOGPATH: "${CI_PROJECT_DIR}/output/logs/"
  BUILD_SCT_OUTPATH: "${CI_PROJECT_DIR}/output/"
  BUILD_SCT_PULLREPO: 'git'
  BUILD_SCT_RUNINDEX: "${CI_NODE_INDEX}"
  BUILD_SCT_RUNTOTAL: "${CI_NODE_TOTAL}"
  BUILD_SCT_RUNURL: "${CI_JOB_URL}/artifacts/raw/"

stages:
  - 'quality'
  - 'build'
  - 'deploy'
  - 'report'

# ----------------------------------------------------------------------------
#                       Job base templates
# ----------------------------------------------------------------------------
.default_job: &default_job
  # Don't download artifacts from previous job
  dependencies: []
  # 'linter' is the default builder for low memory and cpu consumption.
  # 6 'linter' builders, 2 per machine
  tags:
    - 'linter'
  artifacts: &default_artifacts
    paths:
      - "output/logs/*"
    when: always
  only: &default_only
    refs: ['schedules']

# ----------------------------------------------------------------------------
#                         Jobs definition
# ----------------------------------------------------------------------------
quality:linter: &quality_job
  <<: *default_job
  stage: 'quality'
  # 6 'linter' builders, 2 per machine
  parallel: 3
  variables:
    BUILD_SCT_LOGINDEX: 1
  script:
    - "./build.sh -b linter:linter"

quality:test:
  <<: *quality_job
  # 1 'test' builder per nuci5, 2 for the nuci7
  parallel: 4
  tags:
    - 'test'
  variables:
    BUILD_SCT_LOGINDEX: 2
  script:
    - "./build.sh -b test:test"

quality:install:
  <<: *quality_job
  # 2 'install' builders per machine = 2 x vm(2 cpu, 2G mem)
  parallel: 6
  tags:
    - 'install'
  variables:
    BUILD_SCT_LOGINDEX: 3
  script:
    - "./build.sh -f -b install:install -c install:install"

build:
  <<: *default_job
  stage: 'build'
  variables:
    BUILD_SCT_LOGINDEX: 5
  script:
    - "./build.sh -b venv:package"
  artifacts:
    <<: *default_artifacts
    paths:
      - "output/logs/*"
      - "output/dist/*"

# Collect all artifacts from previous jobs and create the final archive
report:
  <<: *default_job
  stage: 'report'
  dependencies:
  variables:
    BUILD_SCT_LOGINDEX: 6
  script:
    - echo "That's all folks !"
  artifacts:
    paths:
      - "output/logs/*"
      - "output/dist/*"
    when: always
  when: always

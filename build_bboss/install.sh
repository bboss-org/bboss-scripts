#!/usr/bin/env bash

# BBOSS Copyright (C) 2019, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later
# https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libinst.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libinst.sh"

# shellcheck source=../lib/libinst_git.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libinst_git.sh"

# shellcheck source=../lib/libinst_docker.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libinst_docker.sh"

# shellcheck source=../lib/libinst_microk8s.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libinst_microk8s.sh"

# shellcheck source=../lib/libinst_python3.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libinst_python3.sh"

#+----------------------------------------------------------------------------
#+                          install.sh variables
#+----------------------------------------------------------------------------
export INSTALL_SCT_PRODUCT='bboss'

#-----------------------------------------------------------------------------
#                   libinst::steps::Depends class
#-----------------------------------------------------------------------------
call libinst::steps::Depends.append_tasks "sshd"

## @fn libinst::steps::Depends::install_sshd()
## @brief       Task configuring sshd in sudo mode.
## @details     The task is called when the `install` command is executed on 
##              the Depends step.
## @param $1    OS distribution name provided by libglb::Version::os_distrib().
## @param $2    OS version number provided by libglb::Version::os_version().
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libinst::steps::Depends
function libinst::steps::Depends::install_sshd() {
  # Usually, host locale environment variables are passed to guest. 
  # It may cause failures if the guest software do not support host locale. 
  call script.display_info "Disabling sshd locale acceptance ..."
  sed -i.old 's/^\(\s*AcceptEnv \(LANG\(UAGE\)\?\|LC_[A-Z]*\).*\)$/#\1/g' \
    /etc/ssh/sshd_config || __retstep__=${?}
  systemctl reload sshd || __retstep__=${?}
  return ${__retstep__}
}

#-----------------------------------------------------------------------------
#                   libinst::steps::Smoke class
#-----------------------------------------------------------------------------
## @class       libinst::steps::Smoke
## @brief       Step running product smoke tests.
## @extends     libsct::Step
## @memberof    libinst
class libinst::steps::Smoke libsct::Step
{
  method    __init__
  method    __del__
  method    "test"
  method    test_generate
  method    test_build
  method    test_create
  method    test_start
  method    test_stop
  method    test_clean
  method    test_up
  method    test_down
  method    test_distclean
}

function libinst::steps::Smoke::__init__() {
  local   -i retval=0
  declare -g smoke_name='smoke'
  declare -g smoke_path smoke_fqdn
  call super.__init__ "${@}" || return ${retval}
  # shellcheck disable=SC1090
  source "${HOME}/.${INSTALL_SCT_PRODUCT}/venv/bin/activate" || retval=${?}
  [[ ${retval} -eq 0 && "${VIRTUAL_ENV}" ]] && return 0
  call script.display_error "Unable to activate virtual env !"
  return ${retval}
}

function libinst::steps::Smoke::__del__() {
  local   -i retval=0
  deactivate    || retval=${?}
  [[ ${retval} -ne 0 ]] && \
    call script.display_error "Unable to deactivate virtual env !"
  call super.__del__ || retval=${?}
  return ${retval}
}

function libinst::steps::Smoke::test() {
  local -i retval=0
  pushd "${HOME}/.${INSTALL_SCT_PRODUCT}" > /dev/null || return ${?}
  smoke_path="${HOME}/.${INSTALL_SCT_PRODUCT}/${smoke_name}/"
  smoke_fqdn="${smoke_name}.${INSTALL_SCT_PRODUCT}.org"
  call self.run_tasks "call ${__class__}.test_%s" || retval=${?}
  popd > /dev/null  || retval=${?}
  rm -rf "${smoke_path}" || retval=${?}
  return ${retval}
}

function libinst::steps::Smoke::test_generate() {
  call script.display_info \
    "Generating project workspace: ${smoke_fqdn} ..."
  bboss generate -q "${smoke_fqdn}" || __retstep__=${?}
  [[ ${__retstep__} -ne 0 ]]   && return ${__retstep__}
  cd "${smoke_path}"  || __retstep__=${?}
  [[ ${__retstep__} -ne 0 ]]   && return ${__retstep__}
  call script.display_info \
    "Displaying docker-compose configuration ..."
  bboss config            || __retstep__=${?}
  [[ ${__retstep__} -ne 0 ]]   && return ${__retstep__}
  call script.display_info \
    "Listing objects... (images + containers + networks + volumes)"
  bboss lso               || __retstep__=${?}
  return ${__retstep__}
}

function libinst::steps::Smoke::test_build() {
  call script.display_info "Building docker images ..."
  bboss build -l    || __retstep__=${?}
  call script.display_info "Listing images ..."
  bboss lsi         || __retstep__=${?}
  return ${__retstep__}
}

function libinst::steps::Smoke::test_create() {
  call script.display_info \
    "Creating docker containers volumes and network ..."
  bboss create      || __retstep__=${?}
  call script.display_info \
    "Listing objects ... (images + containers + networks + volumes)"
  bboss lso         || __retstep__=${?}
  return ${__retstep__}
}

function libinst::steps::Smoke::test_start() {
  call script.display_info "Starting ${smoke_fqdn} stack ..."
  bboss start       || __retstep__=${?}
  call script.display_info "Listing containers ..."
  bboss lsc         || __retstep__=${?}
  return ${__retstep__}
}

function libinst::steps::Smoke::test_stop() {
  call script.display_info "Stopping ${smoke_fqdn} stack ..."
  bboss stop        || __retstep__=${?}
  call script.display_info "Listing containers ..."
  bboss lsc         || __retstep__=${?}
  return ${__retstep__}
}

function libinst::steps::Smoke::test_clean() {
  call script.display_info \
    "Cleaning ${smoke_fqdn} builds (data preserved)"
  bboss clean       || __retstep__=${?}
  call script.display_info \
    "Listing objects ... (images + containers + networks + volumes)"
  bboss lso         || __retstep__=${?}
  return ${__retstep__}
}

function libinst::steps::Smoke::test_up() {
  call script.display_info \
    "Building, creating and starting ${smoke_fqdn} stack ..."
  bboss up          || __retstep__=${?}
  call script.display_info \
    "Listing objects ... (images + containers + networks + volumes)"
  bboss lso         || __retstep__=${?}
  return ${__retstep__}
}

function libinst::steps::Smoke::test_down() {
  call script.display_info \
    "Stopping ${smoke_fqdn} stack, removing containers and network ..."
  bboss down        || __retstep__=${?}
  call script.display_info \
    "Listing objects ... (images + containers + networks + volumes)"
  bboss lso         || __retstep__=${?}
  return ${__retstep__}
}

function libinst::steps::Smoke::test_distclean() {
  call script.display_info \
    "Cleaning ${smoke_fqdn} configuration and data"
  bboss distclean   || __retstep__=${?}
  call script.display_info \
    "Listing objects ... (images + containers + networks + volumes)"
  bboss lso         || __retstep__=${?}
  return ${__retstep__}
}

#-----------------------------------------------------------------------------
#                   libinst::steps::Stack01 class
#-----------------------------------------------------------------------------
## @class       libinst::steps::Stack01
## @brief       Step running product smoke tests.
## @extends     libsct::Step
## @memberof    libinst
class libinst::steps::Stack01 libinst::steps::Smoke
{
  method  __init__
  method  "test"
}
#call libinst::steps::Stack01.append_tasks \
#  "generate" "config" "build" "create" "start" "stop" "clean"
function libinst::steps::Stack01::__init__() {
  call self.set_tasks \
    "generate" "build" "create" "start" "stop" "clean"
  call super.__init__ "${@}" || return ${retval}
}

# Temporary bug fixed : run_command looks for method in final class instead
# than quering the class to get the method.
function libinst::steps::Stack01::test() {
  call super.test "${@}" || return ${retval}
}

#-----------------------------------------------------------------------------
#                   libinst::steps::Stack02 class
#-----------------------------------------------------------------------------
## @class       libinst::steps::Stack02
## @brief       Step running product smoke tests.
## @extends     libsct::Step
## @memberof    libinst
class libinst::steps::Stack02 libinst::steps::Smoke
{
  method  __init__
  method  "test"
}
#call libinst::steps::Stack02.append_tasks \
#  "generate" "config" "up" "down" "distclean"
function libinst::steps::Stack02::__init__() {
  call self.set_tasks \
    "generate" "up" "down" "distclean"
  call super.__init__ "${@}" || return ${retval}
}

# Temporary bug fixed : run_command looks for method in final class instead
# than quering the class to get the method.
function libinst::steps::Stack02::test() {
  call super.test "${@}" || return ${retval}
}

#-----------------------------------------------------------------------------
#                         Script startup
#-----------------------------------------------------------------------------
# Define default steps for bboss install script
call libinst::Script.set_step "stack01" "libinst::steps::Stack01"
call libinst::Script.set_step "stack02" "libinst::steps::Stack02"
#call libinst::Script.set_step "k8s01"   "libinst::steps::K8s01"
#call libinst::Script.set_step "K8s02"   "libinst::steps::K8s02"

# Define default commands for bboss install script
call libinst::Script.set_command -t "test" "stack01" "stack02"
#call libinst::Script.set_command -t "test" "stack01" "stack02" "k8s01" "k8s02"

# Check if the script is executed and not included as a function library with 
# source ./build.sh required for tools/bash_link.sh
if [[ "${0}" == "${BASH_SOURCE[0]}" ]]; then
  # TODO: Temporary patch until object instantiation is ok
  call libinst::Script.__init__ || exit ${?}
  call libinst::Script.run "${@}" || exit ${?}
fi

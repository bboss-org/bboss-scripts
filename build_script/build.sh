#!/usr/bin/env bash

# BBOSS Copyright (C) 2019, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later
# https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libpyt.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libbsh.sh"

#-----------------------------------------------------------------------------
#                         Script startup
#-----------------------------------------------------------------------------
# Check if the script is executed and not included as a function library with 
# source ./build.sh required for tools/bash_link.sh
if [[ "${0}" == "${BASH_SOURCE[0]}" ]]; then
  # TODO: Temporary patch until object instantiation is ok
  call libbsh::Script.__init__ || exit ${?}
  call libbsh::Script.run "${@}" || exit ${?}
fi

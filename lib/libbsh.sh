#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libbld.sh
[[ -z "${__libbld_version__}" ]] && \
  source "$(dirname "${BASH_SOURCE[0]}")/../lib/libbld.sh"

## @file
## @brief       Implementation of libbsh namespace (bash).
## @ingroup     libbld

#+----------------------------------------------------------------------------
#                           libbsh namespace
#-----------------------------------------------------------------------------
## @namespace   libbsh
## @brief       Namespace for bash build classes
## @ingroup     libbld

## @brief       Library version number
# shellcheck disable=SC2034
declare -g __libbsh_version__="1.0"

## @namespace   libbsh::steps
## @memberof    libbsh

#-----------------------------------------------------------------------------
#                    libbsh::steps::Linter class
#-----------------------------------------------------------------------------
## @class       libbsh::steps::Linter
## @extends     libbld::Step
## @memberof    libbsh::steps
class libbsh::steps::Linter libbld::Step
{
  method    __init__
  method    build
}

## @fn libbsh::steps::Linter::__init__()
## @param $1    Command initializing the step.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libbsh::steps::Linter
function libbsh::steps::Linter::__init__() {
  local -a task_lst
  mapfile -t task_lst< <(\
    find . \
    \( -path './coverage' -o -path './output' -o -path './include' \) -prune \
    -o -name '*.sh' -print | sort)
  call self.set_tasks "${task_lst[@]}"
  call super.__init__ "${@}" || return ${?}
}

## @fn libbsh::steps::Linter::build()
## @memberof    libbsh::steps::Linter
function libbsh::steps::Linter::build() {
  # Shellcheck options
  # -a : Include warnings from sourced files
  # -x : Allow 'source' outside of FILES
  # -P : Specify path when looking for sourced files 
  #      ("SCRIPTDIR" for script's dir)
  call self.run_tasks "shellcheck -axP SCRIPTDIR" || return ${?}
}

#-----------------------------------------------------------------------------
#                    libbsh::steps::Test class
#-----------------------------------------------------------------------------
## @class       libbsh::steps::Test
## @extends     libbld::Step
## @memberof    libbsh::steps
class libbsh::steps::Test libbld::Step
{
  method    __init__
  method    build
  method    clean
}

## @fn libbsh::steps::Test::__init__()
## @param $1    Command initializing the step.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libbsh::steps::Test
function libbsh::steps::Test::__init__() {
  declare -gx TEST_SCT_OUTPATH="${BUILD_SCT_OUTPATH}${varsct_run_step}/"
  local -a task_lst
  mapfile -t task_lst < <(find . -type f -name "test_*.sh" | sort)
  call self.set_tasks "${task_lst[@]}"
  call super.__init__ "${@}" || return ${?}
}

## @fn libbsh::steps::Test::build()
## @memberof    libbsh::steps::Test
## @todo        Check shellcheck version
function libbsh::steps::Test::build() {
  call script.display_info 'Configuring output directory ...'
  mkdir -p "${TEST_SCT_OUTPATH}" || retval=${?}
  [[ ${retval} -ne 0 ]] && return ${retval}

  call script.display_info 'Running tests ...'
  export __bashcov_version__
  export FORCE_COLOR
  __bashcov_version__="$(bashcov --version)"
  [[ -z "${NO_COLOR}" ]] && FORCE_COLOR="true"
  bashcov -- "./tools/bash_unit" "${varsct_run_task_lst[@]}" || return ${?}
}

## @fn libbsh::steps::Test::clean()
## @memberof    libbsh::steps::Test
function libbsh::steps::Test::clean() {
  rm -rf "${BUILD_SCT_OUTPATH}coverage*"
  rm -rf "${TEST_SCT_OUTPATH}" || return ${?}
}

#-----------------------------------------------------------------------------
#                         libbsh::Script class
#-----------------------------------------------------------------------------
## @class       libbsh::Script
## @extends     libbld::Script
## @memberof    libbsh
class libbsh::Script libbld::Script

# Define default steps for bash builder script
call libbsh::Script.set_step "linter" "libbsh::steps::Linter"
call libbsh::Script.set_step "test"   "libbsh::steps::Test"

# Define default commands for python builder script
call libbsh::Script.set_command -b "build" "linter" "test"
call libbsh::Script.set_command -c "clean" "test" "linter"

#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libinst.sh
[[ -z "${__libinst_version__}" ]] && \
  source "$(dirname "${BASH_SOURCE[0]}")/../lib/libinst.sh"

## @file
## @brief       Implementation of libinst::steps::Depends for docker.
## @ingroup     libinst

#+----------------------------------------------------------------------------
#                    libinst::steps::Depends class
#-----------------------------------------------------------------------------
call libinst::steps::Depends.append_tasks "docker"

## @fn libinst::steps::Depends::install_docker()
## @brief       Task installing docker in sudo mode.
## @details     The task is called when the `install` command is executed on 
##              the Depends step.
## @param $1    OS distribution name provided by libglb::Version::os_distrib().
## @param $2    OS version number provided by libglb::Version::os_version().
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libinst::steps::Depends
function libinst::steps::Depends::install_docker() {
  local os_distrib="${1}"
  local os_version="${2}"
  local app_version app_name='docker'
  app_version=$(libglb::Version::app "${app_name}")

  if libglb::Version::lt "${app_version}" '18.09.7'; then
    if [[ -z "${app_version}" ]]; then
      call script.display_info "Installing ${app_name^} ..."
    else
      call script.display_info \
        "${app_name^} (${app_version}) requires upgrade, processing ..."
    fi

    local repo="https://download.docker.com/linux/${os_distrib}/"
    local gnupg_pkg=''
    case "${os_distrib}" in
      ('centos')
        # Remove previous version and install Docker CE requirements
        if [[ "${app_version}" ]]; then
          yum -y -q remove \
            docker \
            docker-client \
            docker-client-latest \
            docker-common \
            docker-latest \
            docker-latest-logrotate \
            docker-logrotate \
            docker-engine 2>/dev/null || :;
        fi
        #  Install Docker CE requirements
         yum -y -q install \
          yum-utils \
          device-mapper-persistent-data \
          lvm2 || __retstep__=${?}
        [[ ${__retstep__} -ne 0 ]] && return ${__retstep__}
        #  Install Docker CE
        yum-config-manager -y -q --add-repo "${repo}docker-ce.repo" || \
          __retstep__=${?}
        yum install -y -q docker-ce docker-ce-cli containerd.io || \
          __retstep__=${?}
        systemctl start docker || __retstep__=${?}
        [[ ${__retstep__} -ne 0 ]] && return ${__retstep__}
        chkconfig docker on || __retstep__=${?}
        ;;
      ('debian')
        gnupg_pkg='gnupg2'
        if [[ "${os_version}" == "10" ]]; then
          apt-get --allow-releaseinfo-change update || __retstep__=${?}
        fi
        ;&   #fallthru
      ('ubuntu')
        export DEBIAN_FRONTEND=noninteractive
        apt-get -y -qq update || __retstep__=${?}
        # Remove previous version and install Docker CE requirements
        if [[ "${app_version}" ]]; then
          apt-get -y -q remove \
            docker \
            docker-engine \
            docker.io \
            containerd runc 2>/dev/null || :;
        fi
        #  Install Docker CE requirements
        apt-get -y -qq install \
          apt-transport-https \
          ca-certificates \
          curl \
          ${gnupg_pkg:-'gnupg-agent'} \
          software-properties-common || __retstep__=${?}
        [[ ${__retstep__} -ne 0 ]] && return ${__retstep__}
        # Verify that you now have the key with the fingerprint
        local fingerprint='9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88'
        export APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=DontWarn
        curl -fsSL \
          "https://download.docker.com/linux/${os_distrib}/gpg" | \
          apt-key add - || __retstep__=${?}
        local output
        output=$(apt-key fingerprint 0EBFCD88 | grep -o "${fingerprint}")
        unset APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE
        if [[ "${fingerprint}" != "${output}" ]]; then
          call script.display_error \
            "Docker fingerprint does not match \"${fingerprint}\" !!!"
          echo -e "${output}"
          __retstep__=1
        fi
        [[ ${__retstep__} -ne 0 ]] && return ${__retstep__}
        #  Install Docker CE
        add-apt-repository \
          "deb [arch=amd64] ${repo} $(lsb_release -cs) stable" || __retstep__=${?}
        apt-get -y -qq update || __retstep__=${?}
        apt-get -y -qq install \
          docker-ce docker-ce-cli containerd.io 2>/dev/null || __retstep__=${?}
        ;;
      (*)
        call script.display_error \
          "Unsupported linux distribution : ${os_distrib}"
        __retstep__=1
        ;;
    esac
  fi
  # Docker post-installation steps for Linux
  if [[ ${__retstep__} -eq 0 && -z "$(getent group docker)" ]]; then
    groupadd docker || __retstep__=${?}
  fi
  [[ ${__retstep__} -ne 0 ]] && return ${__retstep__}
  usermod -aG docker "${INSTALL_SCT_PRODUSER}" || __retstep__=${?}
  [[ ${__retstep__} -ne 0 ]] && return ${__retstep__}
  app_version=$(libglb::Version::app "${app_name}")
  call script.display_info "${app_name^} (${app_version}) is installed"
  return ${__retstep__}
}

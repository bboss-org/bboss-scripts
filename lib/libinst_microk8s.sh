#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libinst_snap.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libinst_snap.sh"

## @file
## @brief       Implementation of libinst::steps::Depends for MicroK8s.
## @ingroup     libinst

#-----------------------------------------------------------------------------
#                   libinst::steps::Depends class
#-----------------------------------------------------------------------------
call libinst::steps::Depends.append_tasks "microk8s"

## @fn libinst::steps::Depends::install_microk8s()
## @brief       Task installing microk8s in sudo mode.
## @details     The task is called when the `install` command is executed on 
##              the Depends step.
## @param $1    OS distribution name provided by libglb::Version::os_distrib().
## @param $2    OS version number provided by libglb::Version::os_version().
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libinst::steps::Depends
function libinst::steps::Depends::install_microk8s() {
  local os_distrib="${1}"
  local os_version="${2}"
  local app_version app_name='microk8s'
  app_version=$(libglb::Version::app 'snap list' "${app_name}")

  if ! (snap info "${app_name}" | grep -q 'installed:'); then
    call script.display_info "Installing ${app_name^} ..."
    case "${os_distrib}" in
      ('centos')
        # https://github.com/ubuntu/microk8s/issues/686
        snap install core && \
        snap install microk8s --classic --edge || __retstep__=${?}
        ;;
      ('debian') 
        # https://discuss.kubernetes.io/t/sudo-snap-install-microk8s-classic-fails/4569/2
        # https://github.com/ubuntu/microk8s/issues/679
        snap install core && \
        snap install microk8s --classic --edge || __retstep__=${?}
        ;;
      ('ubuntu')
        snap install microk8s --classic || __retstep__=${?}
        ;;
      (*)
        call script.display_error \
          "Unsupported linux distribution : ${os_distrib}"
        __retstep__=1
        ;;
    esac
    if [[ ${__retstep__} -eq 0 ]]; then
      usermod -a -G microk8s "${INSTALL_SCT_PRODUSER}" || __retstep__=${?}
    fi
  fi
  [[ ${__retstep__} -ne 0 ]] && return ${__retstep__}
  app_version=$(libglb::Version::app 'snap list' "${app_name}")
  call script.display_info "${app_name^} (${app_version}) is installed"
  return ${__retstep__}
}

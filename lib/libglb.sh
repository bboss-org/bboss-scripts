#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libobj.sh
[[ -z "${__libobj_version__}" ]] && \
  source "$(dirname "${BASH_SOURCE[0]}")/../lib/libobj.sh"

## @file
## @brief       Implementation of libglb namespace (generic script).

#+----------------------------------------------------------------------------
#                           libglb namespace
#-----------------------------------------------------------------------------
## @namespace   libglb

## @brief       Library version number
# shellcheck disable=SC2034
declare -g __libglb_version__="1.0"

#-----------------------------------------------------------------------------
#                      libglb::Array class
#-----------------------------------------------------------------------------
## @class       libglb::Array
## @extends     libobj::Object
## @memberof    libglb
class libglb::Array
{
  method    colorize
  method    index
  method    join
  method    reverse
  method    slice_byindex
  method    slice_byvalue
}

## @fn libglb::Array::colorize()
## @brief       Format the array with given ansi color.
## @param $1    Ansi color to use.
## @param ...   Array to colorize.
## @return      Array of colorized lines.
## @memberof    libglb::Array
function libglb::Array::colorize() {
  local -a  txt_lst=("${@:2}")
  [[ ${#txt_lst[@]} -eq 0 ]] && return
  if [[ -n "${NO_COLOR}" ]]; then printf '%s\n' "${txt_lst[@]}"
  else printf "${1}%s\e[39m\e[49m\n" "${txt_lst[@]}"; fi
}

## @fn libglb::Array::index()
## @brief       Returns the index of $1 in ${@:2}.
## @details     Returns the smallest i such that i is the index of the first 
##              occurrence of $1 in the array ${@:2}.
## @param $1    Value to find into the array.
## @param ...   Array to scan.
## @return      The index of $1 into ${@:2} if found, otherwise an empty 
##              string (int 0) on stdout.
## @retval 0
## @memberof    libglb::Array
function libglb::Array::index() {
  local -a array_lst=("${@:2}")
  for i in "${!array_lst[@]}"; do
    [[ "${1}" != "${array_lst[${i}]}" ]] && continue
    echo "${i}"; return
  done
}

## @fn libglb::Array::join()
## @brief       Returns a string which is the concatenation of array items.
## @param $1    Separator to use to concanate array elements.
## @param ...   Array to concanate.
## @return      Returns a string on stdout.
## @retval 0
## @memberof    libglb::Array
function libglb::Array::join() {
  local array_txt=''
  for i in "${@:2}"; do
    [[ "${i}" =~ ^\ *$ ]] && continue
    if [[ -z "${array_txt}" ]]; then array_txt="${i}";
    else array_txt+="${1}${i}"; fi
  done
  echo "${array_txt}"
}

## @fn libglb::Array::reverse()
## @brief       Reverses the order of the items in the array.
## @param ...   Array to reverse.
## @return      Returns the reversed array on stdout.
## @retval 0
## @memberof    libglb::Array
function libglb::Array::reverse() {
  libglb::Array::slice_byindex "-1:0" "${@}"; }

## @fn libglb::Array::slice_byindex()
## @memberof    libglb::Array::
function libglb::Array::slice_byindex() {
  local -a  slice_val=("${1%%:*}" "${1##*:}")
  local -a  slice_lst=("${@:2}")
  local -i  slice_max="$(( ${#slice_lst[@]} - 1 ))"
  [[ ${#slice_val[@]} -ne 2 ]] && return
  [[ ${slice_max} -lt 0 ]] && return
  for i in "${!slice_val[@]}"; do
    [[ ! "${slice_val[${i}]}" =~ ^-?[0-9]+$ ]] && return
    if [[ ${slice_val[${i}]} -lt 0 ]]; then
      slice_val[${i}]="$(( ${#slice_lst[@]} + ${slice_val[${i}]} ))"
      [[ ${slice_val[${i}]} -lt 0 ]] && slice_val[${i}]="0"
    elif [[ ${slice_val[${i}]} -gt ${slice_max} ]]; then
      slice_val[${i}]="${slice_max}"
    fi
  done
  for i in $(eval echo "{${slice_val[0]}..${slice_val[1]}}"); do 
    echo "${slice_lst[${i}]}"
  done 
}

## @fn libglb::Array::slice_byvalue()
## @memberof    libglb::Array
function libglb::Array::slice_byvalue() {
  local     slice_beg slice_end
  local -a  slice_lst=("${@:2}")
  slice_beg=$(libglb::Array::index "${1%%:*}" "${slice_lst[@]}")
  slice_end=$(libglb::Array::index "${1##*:}" "${slice_lst[@]}")
  libglb::Array::slice_byindex "${slice_beg}:${slice_end}" "${slice_lst[@]}"
}

#-----------------------------------------------------------------------------
#                     libglb::Environment class
#-----------------------------------------------------------------------------
## @class       libglb::Environment
## @extends     libobj::Object
## @memberof    libglb
class libglb::Environment
{
  method    expand
  method    dump
}

# (variables are evaluated in alphabetical order)

## @fn libglb::Environment::expand()
## @brief       Expand variable or list of environment variables.
## @param $1    Variable name or glob pattern to include.
## @param $2    Variable name or glob pattern to exclude.
## @memberof    libglb::Environment
function libglb::Environment::expand() {
  local var_name var_value var_flt=${2:-'^$'}
  for var_name in $(eval echo "\${!${1}}"); do
    [[ ${var_name} =~ ${var_flt} ]] && continue
    var_value=\""${!var_name}"\"
    eval "${var_name}=${var_value}"
  done
}

## @fn libglb::Environment::dump()
## @brief       Dump variable or list of environment variables.
## @param $1    Variable name or glob pattern to include.
## @param $2    Variable name or glob pattern to exclude.
## @memberof    libglb::Environment
function libglb::Environment::dump() {
  local var_name var_flt=${2:-'^$'}
  for var_name in $(eval echo "\${!${1}}" | sort); do
    [[ ${var_name} =~ ${var_flt} ]] && continue
    echo -e "export ${var_name}=\x27${!var_name}\x27"
  done
}

#-----------------------------------------------------------------------------
#                      libglb::Version class
#-----------------------------------------------------------------------------
## @class       libglb::Version
## @extends     libobj::Object
## @memberof    libglb
class libglb::Version
{
  method    app
  method    eq
  method    lt
  method    le
  method    ge
  method    gt
  method    os_release
  method    os_distrib
  method    os_version
}

## @fn libglb::Version::app()
## @brief       Return the version number of a command line application.
## @param $1    Command line to run.
## @param $2    Variable name or glob pattern to exclude.
## @memberof    libglb::Version
function libglb::Version::app() {
  local     cmd_line="${1}" app_name="${2:-${1}}"
  local -a  cmd_lst="(${cmd_line[*]})"
  command -v "${cmd_lst[0]}" > /dev/null 2>&1 || return
  [[ ${#cmd_lst[@]} -eq 1 ]] && cmd_line+=' --version'
  ${cmd_line} 2>/dev/null | sed -n "s/${app_name} [^0-9]*\([0-9.]*\).*/\1/pI";
}

## @fn libglb::Version::eq()
## @brief       Check if $1 is a version number equal to $2.
## @param $1    First version number to compare.
## @param $2    Second version number to compare.
## @retval 0    (true) if $1 == $2
## @retval 1    (false) if $1 < $2
## @retval 2    (false) if $1 > $2
## @memberof    libglb::Version
function libglb::Version::eq() {
  if [[ "${#}" -ge "2" ]]; then
    # Split numbers into arrays
    local val_num chk_num fst_num
    declare -a val_lst chk_lst
    mapfile -t val_lst < <(echo "${1/[+-]/.}" | tr '.' '\n')
    mapfile -t chk_lst < <(echo "${2/[+-]/.}" | tr '.' '\n')
    # Complete missing numbers
    while [[ ${#val_lst[@]} -lt ${#chk_lst[@]} ]]; do val_lst+=('0'); done
    while [[ ${#chk_lst[@]} -lt ${#val_lst[@]} ]]; do chk_lst+=('0'); done
    for ndx in "${!val_lst[@]}"; do
      val_num="${val_lst[${ndx}]}"
      chk_num="${chk_lst[${ndx}]}"
      [[ "${val_num}"  =~ ^[0-9]*$ ]] && val_num="$(printf "%04d" "${val_num}")"
      [[ "${chk_num}"  =~ ^[0-9]*$ ]] && chk_num="$(printf "%04d" "${chk_num}")"
      [[ "${val_num}" == "${chk_num}" ]] && continue
      fst_num=$(echo -e "${val_num}\n${chk_num}" | sort -V | head -n 1)
      [[ "${val_num}" ==  "${fst_num}" ]] && return 1 || return 2
    done
  elif [[ "${#}" -eq "1" ]]; then
    return 2  # Lower=2 : Second empty
  fi
  return 0    # Equal=0 : Both empty or both equal
}

## @fn libglb::Version::lt()
## @brief       Check if $1 is a version number lower than $2.
## @param $1    First version number to compare.
## @param $2    Second version number to compare.
## @retval 0    (true)   if $1 < $2.
## @retval 1    (false)  if $1 >= $2.
## @memberof    libglb::Version
function libglb::Version::lt() {
  libglb::Version::eq "${1}" "${2}"; [[ "${?}" -eq "1" ]]
}

## @fn libglb::Version::le()
## @brief       Check if $1 is a version number lower than or equal to $2.
## @param $1    First version number to compare.
## @param $2    Second version number to compare.
## @retval 0    (true)   if $1 <= $2.
## @retval 1    (false)  if $1 > $2.
## @memberof    libglb::Version
function libglb::Version::le() {
  libglb::Version::eq "${1}" "${2}"; [[ "${?}" -ne "2" ]]
}

## @fn libglb::Version::ge()
## @brief       Check if $1 is a version number greater than or equal to $2.
## @param $1    First version number to compare.
## @param $2    Second version number to compare.
## @retval 0    (true)   if $1 >= $2.
## @retval 1    (false)  if $1 < $2.
## @memberof    libglb::Version
function libglb::Version::ge() {
  libglb::Version::eq "${1}" "${2}"; [[ "${?}" -ne "1" ]]
}

## @fn libglb::Version::gt()
## @brief       Check if $1 is a version number greater than $2.
## @param $1    First version number to compare.
## @param $2    Second version number to compare.
## @retval 0    (true)   if $1 > $2.
## @retval 1    (false)  if $1 <= $2.
## @memberof    libglb::Version
function libglb::Version::gt() {
  libglb::Version::eq "${1}" "${2}"; [[ "${?}" -eq "2" ]]
}

## @fn libglb::Version::os_release()
## @memberof    libglb::Version
function libglb::Version::os_release() {
  [[ ! -f "/etc/os-release" ]] && return
  local os_regex="\(\'\([^\']*\)\'\|\"\([^\"]*\)\"\|\([^\"\']*\)\)"
  sed -n "s/^\s*${1}=${os_regex}$/\2\3\4/p" "/etc/os-release"
}

## @fn libglb::Version::os_distrib()
## @memberof    libglb::Version
function libglb::Version::os_distrib() {
  libglb::Version::os_release 'ID'
}

## @fn libglb::Version::os_version()
## @memberof    libglb::Version
function libglb::Version::os_version() {
  libglb::Version::os_release 'VERSION_ID'
}

#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

## @file
## @namespace   libtst

## @brief       Library version number
# shellcheck disable=SC2034
declare -g __libtst_version__="1.0"

#-----------------------------------------------------------------------------
#                        bash_unit helper functions
#-----------------------------------------------------------------------------
function libtst_tst_setup_suite() {
  declare -g vartst_env_prefix="${varsct_env_prefix:-TEST_SCT}"

  declare -g vartst_sct_path vartst_sct_name
  vartst_sct_path=$(dirname "${BASH_SOURCE[1]}")
  vartst_sct_path=$(realpath "${vartst_sct_path}")/
  vartst_sct_name=$(basename "${BASH_SOURCE[1]}")
  vartst_sct_name="${vartst_sct_name%%.*}"

  # Declare and create output directory
  declare -g vartst_out_path="${TEST_SCT_OUTPATH:-${BUILD_SCT_OUTPATH}}"
  if [[ -z "${vartst_out_path}" ]]; then
    vartst_out_path="$(git rev-parse --show-toplevel 2>/dev/null)"
    vartst_out_path="${vartst_out_path:-${PWD}}"
    vartst_out_path+="/output/test/"
  else
    vartst_out_path="${vartst_out_path%/}/"
  fi
  vartst_out_path+="${vartst_sct_name}/"
  mkdir -p "${vartst_out_path}"
}

function libtst_tst_setup() {
  declare -g  vartst_dsp_file=''

  declare -g  libtst_run_opts=''
  declare -gi vartst_run_main=0
  declare -gi vartst_chk_display_line=0
  declare -gi vartst_chk_display_cmd=0

  # Declare and create output directory
  eval "export ${vartst_env_prefix}_OUTPATH=${vartst_out_path}"
#  eval "export ${vartst_env_prefix}_LOGPATH=${vartst_out_path}"
}
#-----------------------------------------------------------------------------
#                         main function call
#-----------------------------------------------------------------------------
function libtst_run_main() {
  # shellcheck disable=SC2154
  vartst_dsp_file="${vartst_out_path}${__bash_unit_current_test__}.dsp"

  # Call build.sh libsct::Script.run function
  # set -o pipefail
  cmd="call ${1}::Script.__init__"
  ( ${cmd}; exit ${?} ) 1> "${vartst_dsp_file}"|| vartst_run_main=${?}
  [[ ${vartst_run_main} -ne 0 ]] && return ${vartst_run_main}
  cmd="call ${1}::Script.run ${libtst_run_opts}${*:2}"
  ( ${cmd}; exit ${?} ) 1> "${vartst_dsp_file}" || vartst_run_main=${?}
  libtst_chk_display_line '#-{79}' 0
  return ${vartst_run_main}
}

#-----------------------------------------------------------------------------
#                       Check display functions
#-----------------------------------------------------------------------------
function libtst_chk_display_hook() {
  # Check if the function is called by the script or by the test directly 
  # to get test the expected result: libtst_chk_display_line_cmd(125)
  # shellcheck disable=SC2154
  [[ -z "${script}" ]] && return 0
  call script.display_info "Inside ${FUNCNAME[1]}()"
}

function libtst_chk_display_line() {
  local    chk_regx="${1}"
  local    chk_text
  local -i chk_line=${2:-${vartst_chk_display_line}}

  # If requested line number is null, search the last matching line from
  # top of the file.
  if [[ ${chk_line} -eq 0 ]]; then
    chk_text=$(grep -nrE "${chk_regx}" "${vartst_dsp_file}" | tail -n1)
    assert_equals "0" "${?}" "'${chk_regx}' not found in whole file !"
    chk_line="${chk_text%%:*}"
  # If requested line number is negative, the index starts at the file tail
  elif [[ ${chk_line} -lt 0 ]]; then
    chk_len=$(wc -l "${vartst_dsp_file}" | grep -oE '^[0-9]+')
    chk_line=$(( chk_line + chk_len + 1 ))
  # If requested line number is positive, the index starts at the file head
  elif [[ -n "${NO_COLOR}" ]]; then
    head -n "${chk_line}" "${vartst_dsp_file}" | tail -n 1 | grep -qE '^$'
    assert_equals "0" "${?}" "Blank line not found in line ${chk_line}"
    (( chk_line ++ ))
  fi
  head -n "${chk_line}" "${vartst_dsp_file}" | \
    tail -n 1 | grep -qE "${chk_regx}"
  assert_equals "0" "${?}" "'${chk_regx}' not found in line ${chk_line}"
  vartst_chk_display_line=$(( chk_line + 1 ))
}

function libtst_chk_display_line_cmd() {
  local     cmd_name="${1}"
  local -a  cmd_step_lst
  local -a  cmd_rang
  eval "cmd_step_lst=(\"\${varsct_cmd_${cmd_name}_lst[@]}\")"
  mapfile -t cmd_rang < <(libglb::Array::slice_byvalue \
    "${2}" "${cmd_step_lst[@]}")

  local stp_init stp_cmd stp_dsp
  vartst_chk_display_cmd=0
  libtst_chk_display_line "\[START] ${cmd_name}\(${cmd_rang[*]})"
  for stp in "${cmd_rang[@]}"; do
    stp_dsp="${stp^}"
    stp_init="libtst::steps::${stp_dsp}::__init__"
    stp_cmd="libtst::steps::${stp_dsp}::${cmd_name}"
    [[ $(type -t "${stp_cmd}") != 'function' ]] && continue
    libtst_chk_display_line "\[INFO ] {3}${stp_dsp}: Initializing ..."
    if [[ $(type -t "${stp_init}") == 'function' ]]; then
      libtst_chk_display_line "\[INFO ] {5}Inside ${stp_init}\()"
    fi
    stp_dsp+=".${cmd_name}\()"
    libtst_chk_display_line "\[START] {3}${stp_dsp}: Running ..."
    libtst_chk_display_line "\[INFO ] {5}Inside ${stp_cmd}\()"
    # Get expected step result
    ${stp_cmd} >/dev/null || vartst_chk_display_cmd="${?}"
    if [[ ${vartst_chk_display_cmd} -eq 0 ]]; then
      libtst_chk_display_line "\[OK   ] {3}${stp_dsp}"
    else
      libtst_chk_display_line "\[ERROR] {3}${stp_dsp}"
      break
    fi
  done
  libtst_chk_display_res "${cmd_name}"
}

function libtst_chk_display_res() {
  local chk_msg chk_res="${vartst_chk_display_cmd}"
  [[ ${vartst_chk_display_cmd} -eq 0 ]] && \
    chk_msg="\[OK   ] {1}${1}\() returned ${chk_res}" || \
    chk_msg="\[ERROR] {1}${1}\() returned ${chk_res}"
  libtst_chk_display_line "${chk_msg}" "${2}" 
}

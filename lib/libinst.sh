#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libsct.sh
[[ -z "${__libsct_version__}" ]] && \
  source "$(dirname "${BASH_SOURCE[0]}")/../lib/libsct.sh"

## @file
## @brief       Implementation of libinst namespace.
## @ingroup     libinst

#+----------------------------------------------------------------------------
#                           libinst namespace
#-----------------------------------------------------------------------------
## @namespace   libinst
## @brief       Namespace for generic install classes.
## @ingroup     libinst

## @namespace   libinst::steps
## @memberof    libinst

## @brief       Library version number
# shellcheck disable=SC2034
declare -g __libinst_version__="1.0"

## @defgroup libinst Install library
## @{
## @var         INSTALL_SCT_CMDARGS
## @brief       Extra command arguments provided through the environment.
## @details     Command arguments stored in this variable have a lower priority
##              than those given on the command line.
## @details     In case of conflict, those given on the command line prevail.
export INSTALL_SCT_CMDARGS
## @var         INSTALL_SCT_GETURL
## @brief       Url used to download the script.
export INSTALL_SCT_GETURL
## @var         INSTALL_SCT_LOGEXT
## @brief       Log files extension.
## @details     When this variable is empty or null, the default value is 
##              '.log'.
export INSTALL_SCT_LOGEXT
## @var         INSTALL_SCT_LOGLEVEL
## @brief       Log level represented by line indentation.
## @details     Command is executed on the first level without any 
##              indentation. INSTALL_SCT_LOGEXT=0 
## @details     Step is executed is on the second level with two spaces
##              indentation. INSTALL_SCT_LOGEXT=1
## @details     Task is executed is on the third level with four spaces
##              indentation. INSTALL_SCT_LOGEXT=2
export INSTALL_SCT_LOGLEVEL
## @var         INSTALL_SCT_LOGPATH
## @brief       Path to store log files when log option is set.
## @details     When this variable is empty or null, the default value is
##              ${INSTALL_SCT_OUTPATH}logs/
export INSTALL_SCT_LOGPATH
## @var         INSTALL_SCT_OUTPATH
## @brief       Path to group install output intermediate and final files.
## @details     When this variable is empty or null, the default value is
##              ${HOME}/.${INSTALL_SCT_PRODUCT}/
export INSTALL_SCT_OUTPATH
## @var         INSTALL_SCT_PRODUCT
## @brief       Name of the product to install.
export INSTALL_SCT_PRODUCT
## @var         INSTALL_SCT_PRODURL
## @brief       Product location url to use for installation.
export INSTALL_SCT_PRODURL
## @var         INSTALL_SCT_PRODUSER
## @brief       Login name of the current user (who is installing the product).
export INSTALL_SCT_PRODUSER
## @var         INSTALL_SCT_PRODVER
## @brief       Version number of the product to install.
export INSTALL_SCT_PRODVER
## @}

#-----------------------------------------------------------------------------
#                   libinst::steps::Depends class
#-----------------------------------------------------------------------------
## @class       libinst::steps::Depends
## @brief       Step installing product dependencies.
## @details     This step runs tasks in sudo mode.
## @extends     libsct::Step
## @memberof    libinst
class libinst::steps::Depends libsct::Step
{
  method    __init__
  method    install
  method    waitfor_srv
}

## @fn libinst::steps::Depends::__init__()
## @param $1    Command initializing the step.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libinst::steps::Depends
function libinst::steps::Depends::__init__() {
  declare -g run_sudo_cmd=""
  if [[ "${EUID}" -ne 0 ]]; then
    run_sudo_cmd="\nexport NO_COLOR=\"${NO_COLOR}\"\n"
    run_sudo_cmd+="export INSTALL_SCT_LOGLEVEL=${varsct_log_ind}\n"
    run_sudo_cmd+="export INSTALL_SCT_PRODUSER=\"${USER}\"\n"
    run_sudo_cmd+="declare -g script=\"${script}\"\n"
    run_sudo_cmd+="declare -g step=\"${self}\"\n"
    run_sudo_cmd+="source \"${0}\"\n"
    [[ -n "${varsct_opt_set_lst['xtrace']}" ]] && run_sudo_cmd+="set -x\n"
    run_sudo_cmd+="call step.__init__ \"${1}\" || exit \${?}\n"
    run_sudo_cmd+="call step.${1} || exit \${?}\n"
    run_sudo_cmd=$(echo -e "${run_sudo_cmd}")
  else
    call super.__init__ "${@}"
  fi
}

## @fn libinst::steps::Depends::install()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libinst::steps::Depends
function libinst::steps::Depends::install() {
  if [[ "${#run_sudo_cmd}" -gt 0 ]]; then
    sudo bash -c "${run_sudo_cmd}" || return ${?}
  else
    declare -g varsct_log_ind="${INSTALL_SCT_LOGLEVEL}"
    local tsk_cmd="${__class__}::install_%s"
    tsk_cmd+=" $(libglb::Version::os_distrib)"
    tsk_cmd+=" $(libglb::Version::os_version)"
    call self.run_tasks "${tsk_cmd}" || return ${?}
  fi
}

## @fn libinst::steps::Depends::waitfor_srv()
## @memberof    libinst::steps::Depends
function libinst::steps::Depends::waitfor_srv() {
  local wait_srv="${1}"
  local wait_msg="${2}"
  local wait_timeout="${3}"

  echo "Waiting for ${wait_srv} during ${wait_timeout} seconds"
  for i in $(seq "${wait_timeout}") ; do
    if systemctl status "${wait_srv}" | grep -q "${wait_msg}"; then
      echo "${wait_srv} started after $(( i - 1 ))s"
      return 0
    fi
    sleep 1
  done
  echo "Operation timed out after ${wait_timeout}s" >&2
  return 1
}

#-----------------------------------------------------------------------------
#                   libinst::steps::Product class
#-----------------------------------------------------------------------------
## @class       libinst::steps::Product
## @brief       Step installing the product.
## @extends     libsct::Step
## @memberof    libinst::steps
class libinst::steps::Product libsct::Step
{
  method    install
}

#-----------------------------------------------------------------------------
#                       libinst::Script class
#-----------------------------------------------------------------------------
## @class       libinst::Script
## @extends     libsct::Script
## @memberof    libinst
class libinst::Script libsct::Script
{
  method    __init__
}

varsct_env_prefix='INSTALL_SCT'

## @fn libinst::Script::__init__()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libinst::Script
function libinst::Script::__init__() {
  # Check distribution compatibility
  local os_name
  os_name="$(libglb::Version::os_distrib) $(libglb::Version::os_version)"
  case "${os_name}" in
    ("centos 7") ;;
    ("debian 9"|"debian 10") ;;
    ("ubuntu 16.04"|"ubuntu 18.04"|"ubuntu 19.04") ;;
#    ("ubuntu 19.10")  ;;
    (*)
      call super.display_error \
        "Unsupported linux distribution : ${os_name}"
      return 1
      ;;
  esac
  # Output and log default variables
  INSTALL_SCT_OUTPATH="${INSTALL_SCT_OUTPATH:-${HOME}/.${INSTALL_SCT_PRODUCT}}/"
  INSTALL_SCT_LOGPATH="${INSTALL_SCT_LOGPATH:-${INSTALL_SCT_OUTPATH}logs/}"
  call super.__init__ "INSTALL_SCT" || return ${?}
}

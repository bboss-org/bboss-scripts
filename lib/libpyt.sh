#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libbld.sh
[[ -z "${__libbld_version__}" ]] && \
  source "$(dirname "${BASH_SOURCE[0]}")/../lib/libbld.sh"

## @file
## @brief       Implementation of libpyt namespace (python).
## @ingroup     libbld

#+----------------------------------------------------------------------------
#                           libpyt namespace
#-----------------------------------------------------------------------------
## @namespace   libpyt
## @brief       Namespace for python build classes
## @ingroup     libbld

## @namespace   libpyt::steps
## @memberof    libpyt

## @brief       Library version number
# shellcheck disable=SC2034
declare -g __libpyt_version__="1.0"

#-----------------------------------------------------------------------------
#                         libpyt::Step class
#-----------------------------------------------------------------------------
## @class       libpyt::Step
## @brief       Step class managing tox project.
## @extends     libbld::Step
## @memberof    libpyt
class libpyt::Step libbld::Step
{
  method    run_tasks
}

## @fn libpyt::Step::run_tasks()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libpyt::Step
function libpyt::Step::run_tasks() {
  if [[ -n "${BUILD_SCT_RUNCI}" ]]; then
    export PATH="${HOME}/.pyenv/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
  fi
  call super.run_tasks \
    "tox --workdir ${BUILD_SCT_OUTPATH}${varsct_run_step}/ -e" || return ${?}
}

#-----------------------------------------------------------------------------
#                    libpyt::steps::Linter class
#-----------------------------------------------------------------------------
## @class       libpyt::steps::Linter
## @extends     libpyt::Step
## @memberof    libpyt::steps
class libpyt::steps::Linter libpyt::Step
{
  method    __init__
  method    build
}

## @fn libpyt::steps::Linter::__init__()
## @param $1    Command initializing the step.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libpyt::steps::Linter
function libpyt::steps::Linter::__init__() {
  call self.set_tasks "flake8" "bandit" "safety"
  call super.__init__ "${@}" || return ${?}
}

## @fn libpyt::steps::Linter::build()
## @memberof    libpyt::steps::Linter
function libpyt::steps::Linter::build() {
  call self.run_tasks || return ${?}
}

#-----------------------------------------------------------------------------
#                    libpyt::steps::Test class
#-----------------------------------------------------------------------------
## @class       libpyt::steps::Test
## @extends     libpyt::Step
## @memberof    libpyt::steps
class libpyt::steps::Test libpyt::Step
{
  method    __init__
  method    build
}

## @fn libpyt::steps::Test::__init__()
## @param $1    Command initializing the step.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libpyt::steps::Test
function libpyt::steps::Test::__init__() {
  call self.set_tasks "py35" "py36" "py" "py37"
  call super.__init__ "${@}" || return ${?}
}

## @fn libpyt::steps::Test::build()
## @memberof    libpyt::steps::Test
function libpyt::steps::Test::build() {
  call self.run_tasks || return ${?}
}

#-----------------------------------------------------------------------------
#                    libpyt::steps::Venv class
#-----------------------------------------------------------------------------
## @class       libpyt::steps::Venv
## @extends     libpyt::Step
## @memberof    libpyt::steps
class libpyt::steps::Venv libpyt::Step
{
  method    build
  method    clean
}

## @fn libpyt::steps::Venv::build()
## @memberof    libpyt::steps::Venv
function libpyt::steps::Venv::build() {
  local -i retval=0

  call script.display_info 'Creating python virtual environment ...'
  virtualenv -p python3 "${BUILD_SCT_OUTPATH}venv/" || retval=${?}
  [[ ${retval} -ne 0 ]] && return ${retval}

  # shellcheck disable=SC1090
  source "${BUILD_SCT_OUTPATH}venv/bin/activate" || retval=${?}
  [[ ${retval} -ne 0 ]] && return ${retval}

  # TODO: Review requirements location
  call script.display_info 'Installing developer requirements ...'
  pip install -r "${varsct_sct_pwd}.dev/req_dev.txt" || retval=${?}
  [[ ${retval} -ne 0 ]] && return ${retval}

  deactivate || retval=${?}
  return ${retval}
}

## @fn libpyt::steps::Venv::clean()
## @memberof    libpyt::steps::Venv
function libpyt::steps::Venv::clean() {
  local -i retval=0
  if [[ "${VIRTUAL_ENV}" == "${BUILD_SCT_OUTPATH}venv/" ]]; then
    deactivate || retval=${?}
    [[ ${retval} -ne 0 ]] && return ${retval}
  fi

  rm -rf "${BUILD_SCT_OUTPATH}venv/" || retval=${?}
  [[ ${retval} -ne 0 ]] && return ${retval}

  return ${retval}
}

#-----------------------------------------------------------------------------
#                  libpyt::steps::Package class
#-----------------------------------------------------------------------------
## @class       libpyt::steps::Package
## @extends     libpyt::Step
## @memberof    libpyt::steps
class libpyt::steps::Package libpyt::Step
{
  method    __init__
  method    build
  method    clean
}

## @fn libpyt::steps::Package::__init__()
## @param $1    Command initializing the step.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libpyt::steps::Package
function libpyt::steps::Package::__init__() {
  declare -ga package_files=(
    "src/${BUILD_SCT_PRODUCT}"
    "setup.cfg"
    "setup.py"
    "README.md"
    "LICENSE"
  )
}

## @fn libpyt::steps::Package::build()
## @memberof    libpyt::steps::Package
function libpyt::steps::Package::build() {
  local -i retval=0 
  local cmd

  call script.display_info 'Configuring output directory ...'
  for f in "${package_files[@]}"; do
    d=$(dirname "${f}")
    if [[ ${d} != '.' ]]; then
      mkdir -p "${BUILD_SCT_OUTPATH}${d}" || retval=${?}
      [[ ${retval} -ne 0 ]] && return ${retval}
    fi
    if [[ -e ${BUILD_SCT_OUTPATH}${f} ]]; then
      echo "${f} already exists"
    else
      ln -s "${varsct_sct_pwd}${f}" "${BUILD_SCT_OUTPATH}${d}" || retval=${?}
    fi
    [[ ${retval} -ne 0 ]] && return ${retval}
  done

  call script.display_info 'Configuring environment ...'
  # shellcheck disable=SC1090
  source "${BUILD_SCT_OUTPATH}venv/bin/activate" || retval=${?}
  if [[ "${BUILD_SCT_GITUSER}" && "${BUILD_SCT_GITEMAIL}" ]]; then
    git config user.name "${BUILD_SCT_GITUSER}" || retval=${?}
    git config user.email "${BUILD_SCT_GITEMAIL}" || retval=${?}
  fi
  [[ ${retval} -ne 0 ]] && return ${retval}

  if [[ "${BUILD_SCT_BUMPVER}" ]]; then
    call script.display_info 'Updating package version ...'
    cmd='bumpversion'
    [[ "${varsct_opt_set_lst['verbose']}" ]] && cmd+=' --verbose'
    [[ "${BUILD_SCT_BUMPVER}" != "build" ]] &&  cmd+=' --tag'
    ${cmd} "${BUILD_SCT_BUMPVER}" || retval=${?}
    [[ ${retval} -ne 0 ]] && return ${retval}
  fi

  call script.display_info 'Building python package ...'
  [[ ${retval} -ne 0 ]] && return ${retval}

    if [[ "${varsct_opt_set_lst['verbose']}" ]]; then 
    pushd "${BUILD_SCT_OUTPATH}" || retval=${?}
  else
    pushd "${BUILD_SCT_OUTPATH}" >/dev/null || retval=${?}
  fi
  cmd="python3 setup.py ${varsct_opt_set_lst['verbose']} sdist bdist_wheel"
  ${cmd} || retval=${?}
    if [[ "${varsct_opt_set_lst['verbose']}" ]]; then 
    popd || retval=${?}
  else
    popd >/dev/null || retval=${?}
  fi
  deactivate

  return ${retval}
}

## @fn libpyt::steps::Package::clean()
## @memberof    libpyt::steps::Package
function libpyt::steps::Package::clean() {
  local -i retval=0

  call script.display_info 'Cleaning build directories ...'
  rm -rf "${BUILD_SCT_OUTPATH}build" "${BUILD_SCT_OUTPATH}dist" \
    "${BUILD_SCT_OUTPATH}src/*.egg-info" || retval=${?}
  [[ ${retval} -ne 0 ]] && return ${retval}

  call script.display_info 'Removing source configuration ...'
  for f in "${package_files[@]}"; do
    rm -f "${BUILD_SCT_OUTPATH}${f}" || retval=${?}
    [[ ${retval} -ne 0 ]] && return ${retval}

    d=$(dirname "${f}");      [[ ${d} == '.' ]] && continue
    d=${BUILD_SCT_OUTPATH}${d}; [[ ! -d "${d}" ]] && continue
    rmdir "${d}" || retval=${?}
    [[ ${retval} -ne 0 ]] && return ${retval}
  done

  return ${retval}
}

#-----------------------------------------------------------------------------
#                  libpyt::steps::Deploy class
#-----------------------------------------------------------------------------
## @class       libpyt::steps::Deploy
## @extends     libpyt::Step
## @memberof    libpyt::steps
class libpyt::steps::Deploy libpyt::Step
{
  method    build
  method    clean
}

## @fn libpyt::steps::Deploy::build()
## @memberof    libpyt::steps::Deploy
function libpyt::steps::Deploy::build() {
  local -i retval=0
  local cmd version
  version=$(call libpyt::Script.version_product)
  [[ -z "${version}" ]] && return 1

  if [[ -z "${BUILD_SCT_PUSHREPO}" ]]; then
    call script.display_error \
      "BUILD_SCT_PUSHREPO must be defined in the environment"
    return 1
  fi
  if [[ -z "${version}" ]]; then
    call script.display_error "Unable to identify package version"
    return 1
  fi

  # shellcheck disable=SC1090
  source "${BUILD_SCT_OUTPATH}venv/bin/activate" || retval=${?}
  [[ ${retval} -ne 0 ]] && return ${retval}

  cmd="twine upload${varsct_opt_set_lst['verbose']:+ --verbose}"
  cmd+=" --repository ${BUILD_SCT_PUSHREPO}"
  if [[ -n "${BUILD_SCT_RUNCI}" ]]; then
    cmd+=" ${BUILD_SCT_OUTPATH}dist/${BUILD_SCT_PRODUCT}-*"
  else
    cmd+=" ${BUILD_SCT_OUTPATH}dist/${BUILD_SCT_PRODUCT}-${version}*"
  fi
  call script.display_info "${cmd}"
  ${cmd} || retval=${?}

  deactivate
  return ${retval}
}

## @fn libpyt::steps::Deploy::clean()
## @memberof    libpyt::steps::Deploy
function libpyt::steps::Deploy::clean() {
  local -i retval=0
  return ${retval}
}

#-----------------------------------------------------------------------------
#                         libpyt::Script class
#-----------------------------------------------------------------------------
## @class       libpyt::Script
## @extends     libbld::Script
## @memberof    libpyt
class libpyt::Script libbld::Script
{
  method    config_value
  method    config_pullrepo
  method    logfile_env
  method    version_product
}

# Define default steps for python builder script
call libpyt::Script.set_step "linter"   "libpyt::steps::Linter"
call libpyt::Script.set_step "test"     "libpyt::steps::Test"
call libpyt::Script.set_step "venv"     "libpyt::steps::Venv"
call libpyt::Script.set_step "package"  "libpyt::steps::Package"
call libpyt::Script.set_step "deploy"   "libpyt::steps::Deploy"

# Define default commands for python builder script
call libpyt::Script.set_command -b "build" \
  "linter" "test" "install" "venv" "package" "deploy"
call libpyt::Script.set_command -c "clean" \
  "deploy" "package" "venv" "install" "test" "linter"

## @fn libpyt::Script::config_value()
## @brief       Get the value of the variable in the configuration file.
## @details     Echo the value of the requested configuration variable from 
##              the specified configuration file's section or an empty string 
##              if not found.
## @param $1    Config file path
## @param $2    Variable's ini section
## @param $2    Variable name
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libpyt::Script
function libpyt::Script::config_value() {
  local -i  retval=0
  local     cfg_cmd
  cfg_cmd="import configparser;cfg=configparser.ConfigParser();"
  cfg_cmd+="cfg.read(\"${1}\"); print(cfg[\"${2}\"][\"${3}\"])"
  cfg_cmd=$(python3 -c "${cfg_cmd}" 2>/dev/null) || retval=${?}
  [[ ${retval} -ne 0 ]] && return ${retval}
  echo "${cfg_cmd}"
}

## @fn libpyt::Script::config_pullrepo()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libpyt::Script
function libpyt::Script::config_pullrepo() {
  # Expected result urls :
  # https://pypi.org/simple/
  # https://test.pypi.org/simple/
  # http://nexus.bbossci.nuci3.local/repository/pypi-group/simple/
  local cfg_url
  cfg_url=$(call self.config_value "${HOME}/.pypirc" "${1}" 'repository')
  [[ -z "${cfg_url}" ]] && return
  # pypi & test.pypi end with legacy for upload
  [[ "${cfg_url}" == "https://upload.pypi.org/legacy/" ]] && \
    cfg_url="https://pypi.org/"
  cfg_url="${cfg_url%/}"
  cfg_url="${cfg_url%/legacy}"
  cfg_url+="/simple/"
}

## @fn libpyt::Script::logfile_env()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libpyt::Script
function libpyt::Script::logfile_env() {
  local -i  retval=0
  local     log_env=''
  local -a  glob_lst log_lst log_tmp
  # Environment variables (Normal Mode & Debug Mode)
  log_env=$(call super.logfile_env)
  # Environment variables (Verbose Mode)
  if [[ ! "${varsct_opt_set_lst['debug']}" && \
        "${varsct_opt_set_lst['verbose']}" ]]; then
    # Pip environment variables
    log_lst=()
    glob_lst=('PIP_*')
    for i in "${glob_lst[@]}"; do
      mapfile -t log_tmp < <(libglb::Environment::dump \
        "${i}" "BUILD_SCT_*")
      log_lst+=("${log_tmp[@]}")
    done
    if [[ ${#log_lst[@]} -gt 0 ]]; then
      log_tmp=('#' '# Pip environment variables :' '#')
      log_env="${log_env:+${log_env}\n}"
      log_env+=$(printf '%s\n' "${log_tmp[@]}")'\n'
      log_env+=$(printf '%s\n' "${log_lst[@]}")
    fi
  fi
  [[ "${log_env}" ]] && printf '%s' "${log_env}"
  return ${retval}
}

## @fn libpyt::Script::version_product()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libpyt::Script
function libpyt::Script::version_product() {
  local     ver_regex="__version__\s*=\s*'\(.*\)'"
  local -a  ver_files
  mapfile -t ver_files < <(grep -l "${ver_regex}" \
    "${BUILD_SCT_GITROOT}src/${BUILD_SCT_PRODUCT}/__version__.py")
  [[ ${#ver_files[@]} -eq 0 ]] && return
  sed -n "s|${ver_regex}|\1|p" "${ver_files[0]}"
}

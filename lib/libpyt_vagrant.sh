#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libpyt.sh
[[ -z "${__libpyt_version__}" ]] && \
  source "$(dirname "${BASH_SOURCE[0]}")/../lib/libpyt.sh"

## @file
## @brief       Implementation of libpyt::steps::Install for vagrant.
## @ingroup     libbld

#+----------------------------------------------------------------------------
#                    libpyt::steps::Install class
#-----------------------------------------------------------------------------
## @class       libpyt::steps::Install
## @brief       Install the final product into a clean environment.
## @details     The Install step aims to run the final product installation
##              script (./install.sh) into a clean environment.
## @extends     libbld::Step
## @memberof    libpyt::steps
class libpyt::steps::Install libbld::Step
{
  method    __init__
  method    build
  method    clean
}
#call libpyt::steps::Install.set_tasks \
#  "centos7" "stretch" "buster" "xenial" "bionic" "disco"

## @fn libpyt::steps::Install::__init__()
## @param $1    Command initializing the step.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libpyt::steps::Install
function libpyt::steps::Install::__init__() {
  call libpyt::steps::Install.set_tasks \
    "centos7" "stretch" "buster" "xenial" "bionic" "disco"
  call super.__init__ "${@}" || return ${?}
}

## @fn libpyt::steps::Install::_vagrant_up()
## @brief       Private callback method for Step::run_tasks() execution.
## @details     The method is called for each items in the task list.
## @param $1    Task to process, here the vagrant virtual machine name.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libpyt::steps::Install
function libpyt::steps::Install::_vagrant_up() {
  local -i retval=0
  call script.display_info "Provisioning ${1} vagrant vm ..."
  bash -c "${__vm_env__}; vagrant up --provision \"${1}\"" || \
    retval=${?}
  if [[ ${retval} -eq 0 ]]; then
    call script.display_info "Running smoke tests on ${1} vagrant vm ..."
    vagrant ssh "${1}" -c \
      "${__vm_env__}; bash \"\${HOME}/.bboss/install.sh\" -t" || \
      retval=${?}
  fi
  vagrant halt "${1}" || retval=${?}
  return ${retval}
}

## @fn libpyt::steps::Install::build()
## @brief       Execute the build command on the Install step.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libpyt::steps::Install
function libpyt::steps::Install::build() {
  local -i retval=0

  call script.display_info 'Configuring output directory ...'
  cp -r "${varsct_sct_pwd}tests/install" "${BUILD_SCT_OUTPATH}" || retval=${?}
  [[ ${retval} -ne 0 ]] && return ${retval}

  # Compute install script url
  if [[ -z "${INSTALL_SCT_GETURL}" ]]; then
    INSTALL_SCT_GETURL="${BUILD_SCT_GITREPO}"
    # git@gitlab.com:bboss-org/bboss.git
    if [[ ${INSTALL_SCT_GETURL} =~ ^git@ ]]; then
      INSTALL_SCT_GETURL="${INSTALL_SCT_GETURL/:/\/}"
      INSTALL_SCT_GETURL="${INSTALL_SCT_GETURL/git@/http:\/\/}"
    fi
    INSTALL_SCT_GETURL="${INSTALL_SCT_GETURL%*.git}/raw/"
    INSTALL_SCT_GETURL+="${BUILD_SCT_GITBRANCH:-${BUILD_SCT_GITCOMMIT}}"
    INSTALL_SCT_GETURL+="/install.sh"
  fi

  # Compute install environment variables
  [[ -z "${INSTALL_SCT_PRODUCT}" ]] && \
    INSTALL_SCT_PRODUCT="${BUILD_SCT_PRODUCT}"
  [[ -z "${INSTALL_SCT_PRODURL}" ]] && \
    INSTALL_SCT_PRODURL=$(call script.config_pullrepo "${BUILD_SCT_PULLREPO}")
  [[ -z "${INSTALL_SCT_PRODVER}" ]] && \
    INSTALL_SCT_PRODVER=$(call script.version_product)

  if [[ -z "${INSTALL_SCT_PRODURL}" && -n "${BUILD_SCT_PULLREPO}" ]]; then
    # If ${BUILD_SCT_PULLREPO} is not found into .pypirc then create the url 
    # from the git repository.
    # https://pip.pypa.io/en/stable/reference/pip_install/#git
    # git@gitlab.com:bboss-org/bboss.git
    if [[ ${BUILD_SCT_GITREPO} =~ ^git@ ]]; then
      INSTALL_SCT_PRODURL="${BUILD_SCT_GITREPO/:/\/}"
      INSTALL_SCT_PRODURL="${INSTALL_SCT_PRODURL/git@/git+http:\/\/}"
    # https://gitlab.com/bboss-org/bboss.git
    else
      INSTALL_SCT_PRODURL="git+${BUILD_SCT_GITREPO}"
    fi
    INSTALL_SCT_PRODURL+="@${BUILD_SCT_GITBRANCH:-${BUILD_SCT_GITCOMMIT}}"
    INSTALL_SCT_PRODURL+="#egg=${INSTALL_SCT_PRODUCT}"
    unset INSTALL_SCT_PRODVER
  fi

  # Run install into a vagrant virtual machine
  pushd "${BUILD_SCT_OUTPATH}install" >/dev/null || retval=${?}
  if [[ ${retval} -eq 0 ]]; then
    local __vm_env__
    __vm_env__=$(libglb::Environment::dump "${INSTALL_SCT_PRODUCT^^}_*")
    __vm_env__+="${__vm_env__:+\n}$(libglb::Environment::dump INSTALL_SCT_*)"
    __vm_env__+="\nexport NO_COLOR=\"${varsct_log_file:+y}\""
    __vm_env__=$(echo -e "${__vm_env__}")
    call self.run_tasks "libpyt::steps::Install::_vagrant_up %s" || retval=${?}
    popd >/dev/null || retval=${?}
  fi
  return ${retval}
}

## @fn libpyt::steps::Install::clean()
## @brief       Execute the `clean` command on the Install step.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libpyt::steps::Install
function libpyt::steps::Install::clean() {
  local -i retval=0
  if [[ ! -e "${BUILD_SCT_OUTPATH}install/Vagrantfile" ]]; then
    call script.display_info \
      "No existing Vagrant environment found in output ..."
    return 0
  fi
  call script.display_info "Destroying existing Vagrant machine ..."
  pushd "${BUILD_SCT_OUTPATH}install" >/dev/null || retval=${?}
  if [[ ${retval} -eq 0 ]]; then
    call self.run_tasks "vagrant destroy -f %s" || return ${?}
    popd >/dev/null || retval=${?}
  fi
  [[ ${retval} -ne 0 ]] && return ${retval}
  call script.display_info "Removing install configuration"
  rm -rf "${BUILD_SCT_OUTPATH}install/" || retval=${?}
  return ${retval}
}

#+----------------------------------------------------------------------------
#                         libpyt::Script class
#-----------------------------------------------------------------------------
call libpyt::Script.set_step "install" "libpyt::steps::Install"

#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libsct.sh
[[ -z "${__libsct_version__}" ]] && \
  source "$(dirname "${BASH_SOURCE[0]}")/../lib/libsct.sh"

## @file
## @brief       Implementation of libbld namespace (generic).
## @ingroup     libbld

#+----------------------------------------------------------------------------
#                           libbld namespace
#-----------------------------------------------------------------------------
## @namespace   libbld
## @brief       Namespace for generic build classes.
## @ingroup     libbld

## @brief       Library version number
# shellcheck disable=SC2034
declare -g __libbld_version__="1.0"

## @defgroup libbld Build library
## @{
## @var         BUILD_SCT_CMDARGS
## @brief       Extra command arguments provided through the environment.
## @details     Command arguments stored in this variable have a lower priority 
##              than those given on the command line.
## @details     In case of conflict, those given on the command line prevail.
export BUILD_SCT_CMDARGS
## @var         BUILD_SCT_GITBRANCH
## @brief       The commit branch name for which product is built.
## @details     Present only when a branch is checked out locally or when the
##              CI builds a specific branch.
export BUILD_SCT_GITBRANCH
## @var         BUILD_SCT_GITCOMMIT
## @brief       The commit revision for which product is built.
export BUILD_SCT_GITCOMMIT
## @var         BUILD_SCT_GITEMAIL
## @brief       The user's email to use when creating a commit or tag during
##              the build.
export BUILD_SCT_GITEMAIL
## @var         BUILD_SCT_GITREPO
## @brief       The URL used to clone the Git repository.
## @details     When this variable is empty or null, the default value is
##              extracted from the `origin` remote.
export BUILD_SCT_GITREPO
## @var         BUILD_SCT_GITROOT
## @brief       Path of the git repository root directory.
## @details     Directory containing the `.git` subdirectory.
export BUILD_SCT_GITROOT
## @var         BUILD_SCT_GITUSER
## @brief       The user's real name to use when creating a commit or tag
##              during the build.
export BUILD_SCT_GITUSER
## @var         BUILD_SCT_LOGEXT
## @brief       Log files extension.
## @details     When this variable is empty or null, the default value is 
##              '.txt' to be able to view the file directly inside firefox web
##              browser and avoid the annoying artifact download specially when
##              Gitlab pages is active.
## @details     When Gitlab pages is not active, whatever the extension,
##              artifact download is the only choice.
export BUILD_SCT_LOGEXT
## @var         BUILD_SCT_LOGINDEX
## @brief       Numeric index of log files to keep execution order.
export BUILD_SCT_LOGINDEX
## @var         BUILD_SCT_LOGLEVEL
## @brief       Log level represented by line indentation.
## @details     Command is executed on the first level without any 
##              indentation. BUILD_SCT_LOGLEVEL=0 
## @details     Step is executed is on the second level with two spaces
##              indentation. BUILD_SCT_LOGLEVEL=1
## @details     Task is executed is on the third level with four spaces
##              indentation. BUILD_SCT_LOGLEVEL=2
export BUILD_SCT_LOGLEVEL
## @var         BUILD_SCT_LOGPATH
## @brief       Path to store log files when log option is set.
## @details     When this variable is empty or null, the default value is
##              ${BUILD_SCT_OUTPATH}logs/
export BUILD_SCT_LOGPATH
## @var         BUILD_SCT_OUTPATH
## @brief       Path to group build output intermediate and final files.
## @details     When this variable is empty or null, the default value is
##              "./output/"
export BUILD_SCT_OUTPATH
## @var         BUILD_SCT_PRODUCT
## @brief       Name of the product to build.
export BUILD_SCT_PRODUCT
## @var         BUILD_SCT_PRODURL
## @brief       Product location url to use for installation.
export BUILD_SCT_PRODURL
## @var         BUILD_SCT_PRODVER
## @brief       Version number of the product to build.
export BUILD_SCT_PRODVER
## @var         BUILD_SCT_PULLREPO
## @brief       Repository identifier to use for product download.
export BUILD_SCT_PULLREPO
## @var         BUILD_SCT_PUSHREPO
## @brief       Repository identifier to use for product deployment.
export BUILD_SCT_PUSHREPO
## @var         BUILD_SCT_RUNCI
## @brief       Identify when the build is executed in CI environment.
## @details     When the build is executed in CI environment, the variable is 
##              not empty.
## @details     Current supported CI environments are Gitlab and Jenkins.
export BUILD_SCT_RUNCI
## @var         BUILD_SCT_RUNINDEX
## @brief       Index of this build job in the job set.
## @details     If the build is not parallelized, this variable is not set.
export BUILD_SCT_RUNINDEX
## @var         BUILD_SCT_RUNTOTAL
## @brief       Total number of instances of this build running in parallel.
## @details     If the job is not parallelized, this variable is assumed to
##              be 1.
export BUILD_SCT_RUNTOTAL
## @var         BUILD_SCT_RUNURL
## @brief       CI server url where artifacts will be uploaded.
export BUILD_SCT_RUNURL
## @}

#-----------------------------------------------------------------------------
#                         libbld::Step class
#-----------------------------------------------------------------------------
## @class       libbld::Step
## @extends     libsct::Step
## @memberof    libbld
class libbld::Step libsct::Step
{
  method    __init__
}

## @fn libbld::Step::__init__()
## @param $1    Command initializing the step.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libbld::Step
function libbld::Step::__init__() {
  declare -g libsct_Step_command="${1}"

  # Check if lists are initialized correctly
  # - Step task list must be full.
  # - Step run list must be empty (if not, task distribution is already done).
  [[ ${#varsct_stp_task_lst[@]} -eq 0 ]] && return
  [[ ${#varsct_run_task_lst[@]} -ne 0 ]] && return

  # ${BUILD_SCT_RUNINDEX} is not well defined, use only one runner
  if [[ ${BUILD_SCT_RUNINDEX} -lt 1 ]]; then
    varsct_run_task_lst=("${varsct_stp_task_lst[@]}")
  elif [[ ${BUILD_SCT_RUNTOTAL} -lt ${BUILD_SCT_RUNINDEX} ]]; then
    return 1
  else
    # ${BUILD_SCT_RUNINDEX} is one based, not zero based
    local -i run_fst run_lst run_inc
    run_fst=$(( BUILD_SCT_RUNINDEX - 1 ))
    run_lst=$(( ${#varsct_stp_task_lst[@]} - 1 ))
    run_inc=${BUILD_SCT_RUNTOTAL}
    for i in $(eval echo "{${run_fst}..${run_lst}..${run_inc}}"); do
      varsct_run_task_lst+=("${varsct_stp_task_lst[${i}]}")
    done
  fi
}

#-----------------------------------------------------------------------------
#                         libbld::Script class
#-----------------------------------------------------------------------------
## @class       libbld::Script
## @extends     libsct::Script
## @memberof    libbld
class libbld::Script libsct::Script
{
  method    __init__

  method    logfile_create
  method    logfile_env

  method    run
}

varsct_env_prefix='BUILD_SCT'

## @fn libbld::Script::__init__()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libbld::Script
# shellcheck disable=SC2034
function libbld::Script::__init__() {
  # CI/CD default variables
  BUILD_SCT_RUNCI="${BUILD_SCT_RUNCI:-${CI:-${JENKINS_HOME}}}"
  BUILD_SCT_RUNINDEX="${BUILD_SCT_RUNINDEX:-${CI_NODE_INDEX}}"
  BUILD_SCT_RUNTOTAL="${BUILD_SCT_RUNTOTAL:-${CI_NODE_TOTAL}}"
  [[ -z "${BUILD_SCT_RUNCI}" ]]     && unset BUILD_SCT_RUNCI
  [[ -z "${BUILD_SCT_RUNINDEX}" ]]  && unset BUILD_SCT_RUNINDEX
  [[ -z "${BUILD_SCT_RUNTOTAL}" ]]  && unset BUILD_SCT_RUNTOTAL

  # Output and log default variables
  BUILD_SCT_OUTPATH="${BUILD_SCT_OUTPATH:-${varsct_sct_pwd}output/}"
  BUILD_SCT_LOGPATH="${BUILD_SCT_LOGPATH:-${BUILD_SCT_OUTPATH}logs/}"
  if [[ -n "${BUILD_SCT_LOGINDEX}" ]]; then
    BUILD_SCT_LOGINDEX="${BUILD_SCT_LOGINDEX}"
  elif [[ -z "${BUILD_SCT_RUNCI}" ]]; then 
    BUILD_SCT_LOGINDEX="1"
  fi
  BUILD_SCT_LOGEXT="${BUILD_SCT_LOGEXT:-txt}"

  # Git repository default variables
  BUILD_SCT_GITUSER="${BUILD_SCT_GITUSER:-${GITLAB_USER_NAME}}"
  BUILD_SCT_GITEMAIL="${BUILD_SCT_GITEMAIL:-${GITLAB_USER_EMAIL}}"
  BUILD_SCT_GITROOT="${BUILD_SCT_GITROOT:-${CI_PROJECT_DIR}}"
  BUILD_SCT_GITCOMMIT="${BUILD_SCT_GITCOMMIT:-${CI_COMMIT_SHA}}"
  BUILD_SCT_GITBRANCH="${BUILD_SCT_GITBRANCH:-${CI_COMMIT_BRANCH}}"
  BUILD_SCT_GITREPO="${BUILD_SCT_GITREPO:-${CI_REPOSITORY_URL}}"

  # Identify Git missing values (local execution)
  [[ -z "${BUILD_SCT_GITUSER}" ]] && \
    BUILD_SCT_GITUSER="$(git config user.name)"
  [[ -z "${BUILD_SCT_GITEMAIL}" ]] && \
    BUILD_SCT_GITEMAIL="$(git config user.email)"
  [[ -z "${BUILD_SCT_GITROOT}" ]] && \
    BUILD_SCT_GITROOT="$(git rev-parse --show-toplevel 2>/dev/null)"
  [[ -z "${BUILD_SCT_GITCOMMIT}" ]] && \
    BUILD_SCT_GITCOMMIT="$(git rev-parse HEAD)"
  [[ -z "${BUILD_SCT_GITBRANCH}" ]] && \
    BUILD_SCT_GITBRANCH="$(git symbolic-ref -q --short HEAD)"

  BUILD_SCT_GITROOT="${BUILD_SCT_GITROOT:+${BUILD_SCT_GITROOT%/}/}"
  [[ -z "${BUILD_SCT_GITBRANCH}" ]] && unset BUILD_SCT_GITBRANCH
  # Identify source repository from current local branch (local execution)
  local git_orig
  if [[ -z "${BUILD_SCT_GITREPO}" ]]; then
    if [[ -z "${BUILD_SCT_GITBRANCH}" ]]; then
      git_orig="origin"
    else
      git_orig="git for-each-ref  --format="
      git_orig+="%(upstream:remotename)"
      git_orig+=" $(git symbolic-ref -q HEAD)"
      git_orig=$(${git_orig})
    fi
    BUILD_SCT_GITREPO=$(git config --get remote."${git_orig}".url)
  fi
  call super.__init__ "BUILD_SCT" || return ${?}
}

## @fn libbld::Script::logfile_create()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libbld::Script
function libbld::Script::logfile_create() {
  # Create the logfile name
  local log_file="${BUILD_SCT_LOGPATH}"
  [[ -n "${BUILD_SCT_LOGINDEX}" ]] && \
    log_file+=$(printf '%0.2d-' "${BUILD_SCT_LOGINDEX}")
  log_file+="${BUILD_SCT_PRODUCT}_${varsct_run_step}_${varsct_run_command}"
  [[ ${BUILD_SCT_RUNINDEX} -gt 0 ]] && \
    log_file+=$(printf '~%0.2d' "${BUILD_SCT_RUNINDEX}")
  log_file+=".${BUILD_SCT_LOGEXT}"
  call super.logfile_create "${log_file}" || return ${?}
  [[ -n "${BUILD_SCT_LOGINDEX}" ]] && (( BUILD_SCT_LOGINDEX ++ ))
  return 0
}

## @fn libbld::Script::logfile_env()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libbld::Script
function libbld::Script::logfile_env() {
  local -i  retval=0
  local     log_env=''
  local -a  glob_lst log_lst log_tmp
  # Environment variables (Normal Mode & Debug Mode)
  log_env=$(call super.logfile_env)
  # Environment variables (Verbose Mode)
  if [[ ! "${varsct_opt_set_lst['debug']}" && \
        "${varsct_opt_set_lst['verbose']}" ]]; then
    # GitLab-ci environment variables
    log_lst=()
    glob_lst=('GITLAB_*' 'CI_*')
    for i in "${glob_lst[@]}"; do
      mapfile -t log_tmp < <(libglb::Environment::dump \
        "${i}" "BUILD_SCT_*")
      log_lst+=("${log_tmp[@]}")
    done
    if [[ ${#log_lst[@]} -gt 0 ]]; then
      log_tmp=('#' '# GitLab-ci environment variables :' '#')
      log_env="${log_env:+${log_env}\n}"
      log_env+=$(printf '%s\n' "${log_tmp[@]}")'\n'
      log_env+=$(printf '%s\n' "${log_lst[@]}")
    fi
    # Jenkins environment variables
    log_lst=()
    glob_lst=('JENKINS_*' 'JOB_*' 'WORKSPACE*' 'NODE_*' 'BUILD_*')
    for i in "${glob_lst[@]}"; do
      mapfile -t log_tmp < <(libglb::Environment::dump \
        "${i}" "BUILD_SCT_*")
      log_lst+=("${log_tmp[@]}")
    done
    if [[ ${#log_lst[@]} -gt 0 ]]; then
      log_tmp=('#' '# Jenkins environment variables :' '#')
      log_env="${log_env:+${log_env}\n}"
      log_env+=$(printf '%s\n' "${log_tmp[@]}")'\n'
      log_env+=$(printf '%s\n' "${log_lst[@]}")
    fi
  fi
  [[ "${log_env}" ]] && printf '%s' "${log_env}"
  return ${retval}
}

## @fn libbld::Script::run()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed task.
## @memberof    libbld::Script
function libbld::Script::run() {
  call super.run "${@}" || exit ${?}
  # Build succeed, copy environment file next to the script
  cp "${BUILD_SCT_OUTPATH}${varsct_sct_name}.env" \
    "${varsct_sct_path}" || exit ${?}
}


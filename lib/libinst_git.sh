#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libinst.sh
[[ -z "${__libinst_version__}" ]] && \
  source "$(dirname "${BASH_SOURCE[0]}")/../lib/libinst.sh"

## @file
## @brief       Implementation of libinst::steps::Depends for git.
## @ingroup     libinst

#-----------------------------------------------------------------------------
#                   libinst::steps::Depends class
#-----------------------------------------------------------------------------
call libinst::steps::Depends.append_tasks "git"

## @fn libinst::steps::Depends::install_git()
## @brief       Task installing git in sudo mode.
## @details     The task is called when the `install` command is executed on 
##              the Depends step.
## @param $1    OS distribution name provided by libglb::Version::os_distrib().
## @param $2    OS version number provided by libglb::Version::os_version().
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libinst::steps::Depends
function libinst::steps::Depends::install_git() {
  local os_distrib="${1}"
  local app_version app_name='git'
  app_version=$(libglb::Version::app "${app_name}")

  if libglb::Version::lt "${app_version}" '2.7.4'; then
    if [[ -z "${app_version}" ]]; then
      call script.display_info "Installing ${app_name^} ..."
    else
      call script.display_info \
        "${app_name^} (${app_version}) requires upgrade, processing ..."
    fi
    case "${os_distrib}" in
      ('centos')
        if [[ "${app_version}" ]]; then
          yum -y -q remove 'git*' || __retstep__=${?}
        fi
        yum -y -q install \
          https://centos7.iuscommunity.org/ius-release.rpm || __retstep__=${?}
        yum -y -q install git2u-all || __retstep__=${?}
        ;;
      ('*')
        call script.display_error \
          "Git upgrade not supported on ${os_distrib}"
        __retstep__=1
        ;;
    esac
  fi
  [[ ${__retstep__} -ne 0 ]] && return ${__retstep__}
  app_version=$(libglb::Version::app "${app_name}")
  call script.display_info "${app_name^} (${app_version}) is installed"
  return ${__retstep__}
}

#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libinst.sh
[[ -z "${__libinst_version__}" ]] && \
  source "$(dirname "${BASH_SOURCE[0]}")/../lib/libinst.sh"

## @file
## @brief       Implementation of libinst::steps for Python3.
## @ingroup     libinst

#-----------------------------------------------------------------------------
#                   libinst::steps::Depends class
#-----------------------------------------------------------------------------
call libinst::steps::Depends.append_tasks "python3"

## @fn libinst::steps::Depends::install_python3()
## @brief       Task installing python3 in sudo mode.
## @details     The task is called when the `install` command is executed on 
##              the Depends step.
## @param $1    OS distribution name provided by libglb::Version::os_distrib().
## @param $2    OS version number provided by libglb::Version::os_version().
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libinst::steps::Depends
function libinst::steps::Depends::install_python3() {
  local os_distrib="${1}"
  local app_version app_name='Python3 requirements'
  app_version=$(libglb::Version::app 'python3' 'python')

  call script.display_info "Installing ${app_name^} ..."
  case "${os_distrib}" in
    ('centos')
      yum -y -q install centos-release-scl || __retstep__=${?}
      yum -y -q install rh-python36 || __retstep__=${?}
      # shellcheck disable=SC1091
      source '/opt/rh/rh-python36/enable'
      ;;
    ('ubuntu')
      add-apt-repository universe || __retstep__=${?}
      ;&   #fallthru
    ('debian')
      apt-get -y -qq install \
        python3-pip virtualenv 2>/dev/null || __retstep__=${?}
      ;;
    (*)
      call script.display_error \
        "Unsupported linux distribution : ${os_distrib}"
      __retstep__=1
      ;;
  esac
  [[ ${__retstep__} -ne 0 ]] && return ${__retstep__}
  app_version=$(libglb::Version::app 'python3' 'python')
  call script.display_info "${app_name^} (${app_version}) are installed"
  return ${__retstep__}
}

#-----------------------------------------------------------------------------
#                    libinst::steps::Product class
#-----------------------------------------------------------------------------
## @fn libinst::steps::Product::install()
## @brief       Step method installing the python product.
## @details     The method is called when the `install` command is executed on 
##              the Product step.
## @param $1    OS distribution name provided by libglb::Version::os_distrib().
## @param $2    OS version number provided by libglb::Version::os_version().
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libinst::steps::Product
function libinst::steps::Product::install() {
  local pip_url="${INSTALL_SCT_PRODURL}"
  local pip_ver="${INSTALL_SCT_PRODVER}"
  local pip_host='' pip_cmd='pip install -q'

  call script.display_info "Installing ${INSTALL_SCT_PRODUCT^} ..."

  # Setup the python virtual environment if it does not already exist
  if [[ ! -d "${HOME}/.${INSTALL_SCT_PRODUCT}/venv" ]]; then
    case "$(libglb::Version::os_distrib)" in
      ('centos')
        # shellcheck disable=SC1091
        source '/opt/rh/rh-python36/enable'
        ;;
    esac
    virtualenv -p python3 "${HOME}/.${INSTALL_SCT_PRODUCT}/venv" || __retstep__=${?}
  fi
  # Activate the python virtual environment
  if [[ ${__retstep__} -eq 0 ]]; then
    # shellcheck disable=SC1090
    source "${HOME}/.${INSTALL_SCT_PRODUCT}/venv/bin/activate"
  fi

  # Install the python product in the virtual environment
  if [[ "${VIRTUAL_ENV}" ]]; then
    ${pip_cmd} --upgrade pip || __retstep__=${?}

    # Add specified pip repository (git source or pypi alternative)
    pip_ver=" ${INSTALL_SCT_PRODUCT}${pip_ver:+==${pip_ver}}"
    if [[ ${pip_url} =~ ^git+ ]]; then
      # -e to keep sources inside virtualenv subdirectory instead than inside
      # a /tmp subdirectory (easier to clean as servers are not rebooted)
      pip_cmd+=" --force-reinstall -e ${pip_url}"
      pip_ver=""

    # For http protocol it should be a nexus server (pypi proxy required)
    elif [[ ${pip_url} =~ ^http:// ]]; then 
      pip_host="${pip_url#http://*}"
      pip_host="${pip_host%%/*}"
      pip_host="--trusted-host ${pip_host%%:*}"
      pip_cmd+=" --index-url ${pip_url} ${pip_host}"

    # test.pypi.org must be an extra index to be able to load dependencies
    elif [[ "${pip_url}" == "https://test.pypi.org/simple/" ]]; then
      pip_cmd+=" --extra-index-url ${pip_url}"

    # pypi.org and nexus group must be the main index
    elif [[ "${pip_url}" ]]; then
      pip_cmd+=" --index-url ${pip_url/upload.pypi.org/pypi.org}"
    fi

    # 'PyYAML>=3.10,<4.3' are docker-compose requirements colliding with 
    # ansible. (pip is unable to resolve the rule from setup.py)
    # https://github.com/pypa/pip/issues/988
    pip_cmd+="${pip_ver}"
    echo "${pip_cmd} 'PyYAML>=3.10,<4.3'"
    ${pip_cmd} 'PyYAML>=3.10,<4.3' || __retstep__=${?}
    deactivate
  else
    __retstep__=1
  fi

  return ${__retstep__}
}

# Define default steps for python install script
call libinst::Script.set_step "depends" "libinst::steps::Depends"
call libinst::Script.set_step "product" "libinst::steps::Product"

# Define default commands for python install script
call libinst::Script.set_command -i "install" "depends" "product"

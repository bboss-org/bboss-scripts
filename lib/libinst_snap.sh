#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libinst.sh
[[ -z "${__libinst_version__}" ]] && \
  source "$(dirname "${BASH_SOURCE[0]}")/../lib/libinst.sh"

## @file
## @brief       Implementation of libinst::steps::Depends for snap.
## @ingroup     libinst

#-----------------------------------------------------------------------------
#                   libinst::steps::Depends class
#-----------------------------------------------------------------------------
call libinst::steps::Depends.append_tasks "snap"

## @fn libinst::steps::Depends::install_snap()
## @brief       Task installing snap in sudo mode.
## @details     The task is called when the `install` command is executed on 
##              the Depends step.
## @param $1    OS distribution name provided by libglb::Version::os_distrib().
## @param $2    OS version number provided by libglb::Version::os_version().
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libinst::steps::Depends
function libinst::steps::Depends::install_snap() {
  local os_distrib="${1}"
  local os_version="${2}"
  local app_version app_name='snap'
  app_version=$(libglb::Version::app "${app_name}")

  if [[ -z $(command -v "${app_name}") ]]; then
    call script.display_info "Installing ${app_name^} ..."
    case "${os_distrib}" in
      ('centos')
        # Do not test 'yum install epel-release' returned value
        # Public key for epel-release-7-11.noarch.rpm is not installed
        yum install -y -q epel-release
        yum install -y -q snapd && \
        systemctl enable --now snapd.socket && \
        ln -s /var/lib/snapd/snap /snap || __retstep__=${?}
        ;;
      ('debian'|'ubuntu')
        apt-get -y -qq update && \
        apt-get -y -qq install snapd || __retstep__=${?}
        ;;
      (*)
        call script.display_error \
          "Unsupported linux distribution : ${os_distrib}"
        __retstep__=1
        ;;
    esac
    [[ ${__retstep__} -ne 0 ]] && return ${__retstep__}
    # Commands not supported on debian stretch :
    # Unit snapd.seeded.service could not be found.
    if ! [[ "${os_distrib} ${os_version}" == 'debian 9' ]]; then
      # Start snapd.seeded.service if not already started (Centos7)
      if ! systemctl status snapd.seeded.service; then
        systemctl start snapd.seeded.service || __retstep__=${?}
        call self.waitfor_srv 'snapd.seeded.service' \
          'Active: active' 10 || __retstep__=${?}
      fi
    fi
  fi
  [[ ${__retstep__} -ne 0 ]] && return ${__retstep__}
  app_version=$(libglb::Version::app "${app_name}")
  call script.display_info "${app_name^} (${app_version}) is installed"
  return ${__retstep__}
}

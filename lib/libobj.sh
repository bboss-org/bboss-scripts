#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

## @file
## @brief       Implementation of libobj namespace (OOP).

#+----------------------------------------------------------------------------
#                           libobj namespace
#-----------------------------------------------------------------------------
## @namespace   libobj

## @brief       Bash coverage pragma to disable PS4 usage.
## @details     bashcov also uses PS4 to track executed lines, defining PS4 in 
##              the source code breaks coverage results.
# shellcheck disable=SC2154
export __libobj_bashcov__="${__bashcov_version__}"
[[ -z "${__libobj_bashcov__}" ]] && export PS4='+ ${FUNCNAME[0]:-main}() '

## @brief       Bash link pragma to disable main code execution.
## @details     It avoid class declaration to be executed when files are linked.
# shellcheck disable=SC2154
export __libobj_bashlnk__="${__bash_link__}"

## @brief       Library version number
# shellcheck disable=SC2034
declare -g __libobj_version__="1.0"

## @brief       Library debug flags
# shellcheck disable=SC2034
declare -g __libobj_debug__="+x"

#-----------------------------------------------------------------------------
#                           Command keywords
#-----------------------------------------------------------------------------
## @private
## @brief       Pointer to instance class.
## @details     This pointer must only be used inside a class method to call
##              other method with the same instance.
## @details     The global definition is never used and must stay empty, as
##              `self` is always defined on the stack by `call` (local defined)
##              before calling the given class method.
## @details     However the global definition avoid the shellcheck SC2154
##              warning everywhere :(`self` is referenced but not assigned).
## @memberof    libobj::Object
declare -g self=""
## @private
## @brief       Pointer to instance base class.
## @details     This pointer must only be used inside a class method to call
##              base class methods with the same instance.
## @details     The global definition is never used and must stay empty, as 
##              `super` is always defined on the stack by `call` (local defined)
##              before calling the given class method.
## @details     However the global definition avoid the shellcheck SC2154
##              warning everywhere :(`super` is referenced but not assigned).
## @memberof    libobj::Object
declare -g super=""

## @fn class()
## @brief       Keyword used to declare a new class.
## @param $1    Class name including namespace.
## @param ...   Base class list (space separated)
## @relatesalso libobj::Type
function class() {
  [[ -n "${__libobj_bashlnk__}" ]] && return
  declare -g  libobj_class_name="${1}"
  libobj::Type::__new__ "${@}"
}

## @fn method()
## @brief       Keyword used to declare a new method in a class.
## @param $1    Method name
## @relatesalso libobj::Type
function method() {
  [[ -n "${__libobj_bashlnk__}" ]] && return
  libobj::Type::add_method "${libobj_class_name}" "method" "${1}"
}

## @fn property()
## @brief       Keyword used to declare a new property in a class.
## @param $1    Property name
## @relatesalso libobj::Type
function property() {
  [[ -n "${__libobj_bashlnk__}" ]] && return
  libobj::Type::add_method "${libobj_class_name}" "property" "${@}" 
}

## @fn new()
## @brief       Keyword used to create a new instance.
## @param $1    Instance name (variable holding the instance).
## @param $2    Class name (including namespace).
## @param ...   Parameters adressed to the __init__ method (implementation 
##              specific)
## @relatesalso libobj::Object
## @todo        Check instance integrity (all declared methods exists in whole
##              class hierarchy.
function new() {
  [[ -n "${__libobj_bashlnk__}" ]] && return
  local __this__="${1}"
  local __class__="${2}"
  # Check variable name convention
  if [[ ! "${__this__}" =~ ^[a-z_] ]]; then
    libobj::Type::raise_exit "${FUNCNAME[0]}" \
      "Variable instance name must start with a lowercase or an" \
      "underline character!"
  elif [[ ! "${__this__}" =~ ^[a-z_][a-z_0-9]*$ ]]; then
    libobj::Type::raise_exit "${FUNCNAME[0]}" \
      "Variable instance name must contain only lowercase, numbers or" \
      "underline characters!"
  fi
  # Check instance's class existence.
  if ! declare -p "${__class__//::/_}" >/dev/null 2>&1; then 
    libobj::Type::raise_exit "${FUNCNAME[0]}" \
      "The class ${__class__} does not exist!"
  fi
  # Create the instance and call the __init__ method.
  eval "declare -g ${__this__}=\"${__class__}/${__class__}/${__this__}\""
  call "${__this__}.__init__" "${@:3}"
  return ${?}
}

## @fn call()
## @brief       Keyword used to call an instance method or property.
## @param $1    Instance method identifier (instance.method)
## @param ...   Arguments passed to the method.
## @relatesalso libobj::Object
# shellcheck disable=SC2034
function call() {
  [[ -n "${__libobj_bashlnk__}" ]] && return
  local __debug__
   # Do not change xtrace flags when bashcov is running
  __debug__="$(shopt -po xtrace)"
  [[ -z "${__libobj_bashcov__}" ]] && set "${__libobj_debug__}"
  local __this__="${1%%.*}" # Class or instance, word before the dot.
  local __meth__="${1#*.}"  # Method name, word after the dot.
  # Check syntax of fist parameter instance.method or class.method
  if [[ ! "${1}" =~ ^.+\..+$ ]]; then 
    libobj::Type::raise_exit "${FUNCNAME[0]}" \
      "Bad syntax"
  # If ${__this__} is a valid variable name, extract the variable content
  elif [[ "${__this__}" =~ ^[a-z_][a-z0-9_]*$ ]]; then
    __this__="${!__this__}"
  fi
  local __class__="${__this__%%/*}" # Instance class, first word.
  local __call__="${__this__#*/}"   # Class to call, word between slashes.
  __call__="${__call__%/*}"
  __this__="${__this__##*/}"        # Instance name, last word.
  [[ "${__this__}" == "${__call__}" ]] && __this__=""
  # TODO : Check class existence
  __call__=$(libobj::Type::resolve_method "${__meth__}" "${__call__}")
  # Method not found, raise the exception on the callee class.
  if [[ -z "${__call__}" ]]; then 
    libobj::Type::raise_exit "${FUNCNAME[0]}" \
      "Mehod ${1} not found"
  fi
  # Call the method
  local -a _bases__
  mapfile -t  __bases__ < <( eval echo "\${${__call__//::/_}[__bases__]}" )
  local super="${__class__}/${__bases__[0]}/${__this__}"
  local self="${__class__}/${__class__}/${__this__}"
  [[ -z "${__bases__[0]}" ]] && super=""
  ${__debug__}
  "${__call__}::${__meth__}" "${@:2}"
  return ${?}
}

#+----------------------------------------------------------------------------
#                           Type class
#-----------------------------------------------------------------------------
## @class       libobj::Type
## @memberof    libobj

## @fn libobj::Type::__new__()
## @brief       Type constructor (class factory).
## @param $1    Class name (including namespace).
## @param ...   Base class name list (including namespace).
## @memberof    libobj::Type
function libobj::Type::__new__() {
  local     __class__="${1}"
  local -a  __bases__=("${@:2}")
  local     __dict__="${__class__//::/_}"
  # Class name must follow UpperCamelCase convention
  if [[ ! "${__class__##*::}" =~ ^[A-Z] ]]; then
    libobj::Type::raise_exit "${FUNCNAME[1]}" \
      "Class name must start with an uppercase letter!"
  elif [[ ! "${__class__##*::}" =~ ^[A-Z][A-Za-z0-9]*$ ]]; then
    libobj::Type::raise_exit "${FUNCNAME[1]}" \
      "Class name must contain only alphabetical characters" \
      "or numbers!"
  # Check class is not already defined.
  elif declare -p "${__dict__}" >/dev/null 2>&1; then 
    libobj::Type::raise_exit "${FUNCNAME[1]}" \
      "The class ${__class__} already exists!"
  fi
  # Check for base class existence
  local cls
  for cls in "${__bases__[@]}"; do
    if ! declare -p "${cls//::/_}" >/dev/null 2>&1; then
      libobj::Type::raise_exit "${FUNCNAME[1]}" \
        "Unknown base class: ${cls}"
    fi
  done
  # Force libobj::Object to be the base class when none is defined
  if [[ "${#__bases__[@]}" -eq 0 ]]; then
      [[ "${__class__}" !=  "libobj::Object" ]] && __bases__=("libobj::Object")
  fi
  eval "declare -gA ${__dict__}"
  eval "${__dict__}[__bases__]=\"${__bases__[*]}\""
}

## @fn libobj::Type::add_method()
## @brief       Add a method to the class.
## @param $1    Class name (including namespace).
## @param $2    Method type `method` or `property`
## @param $3    Method name
## @param ...   Property specific parameters
## @memberof    libobj::Type
function libobj::Type::add_method() {
  local __class__="${1}"
  local __type__="${2}"
  local __name__="${3}"
  local __dict__="${__class__//::/_}"
  # Check the class is defined
  if ! declare -p "${__dict__}" >/dev/null 2>&1; then 
    libobj::Type::raise_exit "${FUNCNAME[1]}" \
      "The class ${__class__} does not exist!"
  # Check method name convention
  elif [[ ! "${__name__}" =~ ^[A-Za-z_] ]]; then
    libobj::Type::raise_exit "${FUNCNAME[1]}" \
      "The name of a ${__type__} must start with an alphabetical or an" \
      "underline character!"
  elif [[ ! "${__name__}" =~ ^[A-Za-z_][A-Za-z_0-9]*$ ]]; then
    libobj::Type::raise_exit "${FUNCNAME[1]}" \
      "The name of a ${__type__} must contain only alphabetical," \
      "number or underline characters!"
  fi
  # Check method is not already defined.
  local meth_type
  meth_type=$(eval echo "\${${__dict__}[${__name__}]}") || exit 255
  if [[ -n "${meth_type}" ]]; then
    libobj::Type::raise_exit "${FUNCNAME[1]}" \
      "A ${meth_type} is already defined with this name!"
  fi
  # Add the property to the class.
  eval "${__dict__}[${__name__}]=\"${__type__}\""
}

## @fn libobj::Type::resolve_method()
## @brief       Find in the list of class hierarchy, the class implementing
##              the requested method.
## @param $1    Name of the method to find.
## @param ...   List of classes hierachy (list of classes).
## @return      The name of the class implementing the method is printed on
##              stdout.
## @retval 0    (true)  If the method has been found in one of the class or
##                      its parent hierarchy.
## @retval 1    (false) If the method has not been found.
## @memberof    libobj::Type
## @todo        Implement Python MRO (Method Resolution Order).
function libobj::Type::resolve_method() {
  # Multi-inheritance is implemented but not required yet, when it will be,
  # have a look to Python MRO (Method Resolution Order)
  local cls
  for cls in "${@:2}"; do
    # Ensure we have not an empty class name
    [[ -z "${cls}" ]] && exit 255
    # Check if the method exits in this class
    local meth_type
    meth_type=$(eval echo "\${${cls//::/_}[${1}]}")
    if [[ "${meth_type}" =~ ^method ]]; then
      echo "${cls}"; return 0
    fi
    # Search in base classes if there is any
    local -a base_list
    mapfile -t base_list < <( eval echo "\${${cls//::/_}[__bases__]}" )
    if [[ "${#base_list[@]}" -gt 0 ]]; then
      cls=$(libobj::Type::resolve_method "${1}" "${base_list[@]}")
    # Next class in the list
    else
      cls=""
    fi
    # Continue until found
    [[ -z "${cls}" ]] && continue
    echo "${cls}"; return 0
  done
  exit 255
}

## @fn libobj::Type::raise_exit()
## @brief       Exit the script with error code 255.
## @details     Display source, method name and error message on stderr and 
##              exit the script with an error code 255.
## @param $1    Name of the method issuing the termination.
## @param ...   Message strings to concatenate.
## @retval 255  Fixed error code, mainly used during bash linking.
## @memberof    libobj::Type
function libobj::Type::raise_exit() {
  echo "${BASH_SOURCE[0]}: ${1}(): ${*:2}" >&2
  local frame=0
  while caller $frame; do
    ((frame++));
  done
  exit 255
}

#+----------------------------------------------------------------------------
#                         Object base class
#-----------------------------------------------------------------------------
## @class       libobj::Object
## @memberof    libobj
class libobj::Object
{
  method    __init__
  method    __del__
}

## @fn libobj::Object::__init__()
## @brief       Initialize new created instance.
## @details     Called after the instance has been created (new()), but before
##              it is returned to the caller.
## @details     If a base class has an __init__() method, the derived class’s
##              __init__() method, if any, must explicitly call it to ensure
##              proper initialization of the base class part of the instance;
##              for example: call super.__init__([args...]).
## @param ...   The arguments are those passed to the class constructor
##              expression.
## @retval      Method returned value.
## @memberof    libobj::Object
function libobj::Object::__init__() {
  :
}

## @fn libobj::Object::__del__()
## @brief       Called when the instance is about to be destroyed.
## @details     This is also called a finalizer or a destructor.
## @details     If a base class has a __del__() method, the derived class’s
##              __del__() method, if any, must explicitly call it to ensure
##              proper deletion of the base class part of the instance.
## @retval      Method returned value.
## @memberof    libobj::Object
function libobj::Object::__del__() {
  :
}

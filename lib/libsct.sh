#!/usr/bin/env bash

## @copyright   BBOSS Copyright (C) 2019, Laurent Marchelli
## @copyright   SPDX-License-Identifier: GPL-3.0-or-later
## @copyright   https://gitlab.com/bboss-org/bboss-scripts

# shellcheck source=../lib/libglb.sh
[[ -z "${__libglb_version__}" ]] && \
  source "$(dirname "${BASH_SOURCE[0]}")/../lib/libglb.sh"

## @file
## @brief       Implementation of libsct namespace (generic script).
## @ingroup     libsct

#+----------------------------------------------------------------------------
#                           libsct namespace
#-----------------------------------------------------------------------------
## @namespace   libsct
## @brief       Namespace for generic script classes.
## @ingroup     libsct

## @brief       Library version number
# shellcheck disable=SC2034
declare -g __libsct_version__="1.0"

#-----------------------------------------------------------------------------
#                         libsct::Step class
#-----------------------------------------------------------------------------
## @class       libsct::Step
## @extends     libobj::Object
## @memberof    libsct
class libsct::Step
{
  method    __init__
  method    set_tasks
  method    append_tasks
  method    insert_tasks
  method    run_tasks
}

## @fn libsct::Step::__init__()
## @param $1    Command initializing the step.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Step
function libsct::Step::__init__() {
  declare -g libsct_Step_command="${1}"
  varsct_run_task_lst=("${varsct_stp_task_lst[@]}")
}

## @fn libsct::Step::set_tasks()
## @brief       Set the tasks list.
## @param ...   New task list.
## @memberof    libsct::Step
function libsct::Step::set_tasks() {
  # shellcheck disable=SC2034
  varsct_stp_task_lst=("${@}")
}

## @fn libsct::Step::append_tasks()
## @brief       Append tasks at the end of the current task list.
## @param ...   Tasks to append to the current list.
## @memberof    libsct::Step
function libsct::Step::append_tasks() {
  # shellcheck disable=SC2034
  varsct_stp_task_lst+=("${@}")
}

## @fn libsct::Step::insert_tasks()
## @brief       Insert tasks at the beginning of the current task list.
## @param ...   Tasks to insert in the current list.
## @memberof    libsct::Step
function libsct::Step::insert_tasks() {
  # shellcheck disable=SC2034
  varsct_stp_task_lst=("${varsct_stp_task_lst[@]}" "${@}")
}

## @fn libsct::Step::run_tasks()
## @memberof    libsct::Step
function libsct::Step::run_tasks() {
  local    tsk_display
  local    tsk_command
  local -i retval=0 tsk_retval
  local -i __retstep__=0
  for varsct_run_task in "${varsct_run_task_lst[@]}"; do
    tsk_display="${__class__##*:}.${libsct_Step_command}(${varsct_run_task})"
    call script.display_start "${tsk_display}: Running..."
    (( varsct_log_ind += 2 ))
    tsk_retval=0
    [[ "${*}" =~ %s ]] && \
      tsk_command="${*//\%s/${varsct_run_task}}" || \
      tsk_command="${*} ${varsct_run_task}"
    ${tsk_command} || tsk_retval=${?}
    (( varsct_log_ind -= 2 ))
    if [[ ${tsk_retval} -eq 0 ]]; then
      call script.display_ok    "${tsk_display}"
    else
      call script.display_error "${tsk_display} returned ${tsk_retval}"
      [[ ${retval} -eq 0 ]] && retval=${tsk_retval}
    fi
    [[ ${__retstep__} -ne 0 ]] && break
  done
  return ${retval}
}

#+----------------------------------------------------------------------------
#                          libsct::Script class
#-----------------------------------------------------------------------------
## @class       libsct::Script
## @extends     libobj::Object
## @memberof    libsct
class libsct::Script
{
  method    __init__
  method    set_step
  method    set_command

  method    display_start
  method    display_info
  method    display_ok
  method    display_error

  method    dump
  method    dump_header
  method    dump_env

  method    env_check

  method    logfile_create
  method    logfile_env

  method    parse_args

  method    run
  method    run_command
}

## @memberof    libsct::Script
declare -g  varsct_env_prefix='SCRIPT_SCT'
## @memberof    libsct::Script
declare -ga varsct_env_lst

## @memberof    libsct::Script
declare -g  varsct_sct_pwd
## @memberof    libsct::Script
declare -g  varsct_sct_path
## @memberof    libsct::Script
declare -g  varsct_sct_name
## @memberof    libsct::Script
declare -g  varsct_sct_ext
varsct_sct_pwd="${PWD}/"
varsct_sct_path=$(dirname "${0}")
varsct_sct_path=$(realpath "${varsct_sct_path}")/
varsct_sct_name=$(basename "${0}")
varsct_sct_ext="${varsct_sct_name##*.}"
varsct_sct_name="${varsct_sct_name%%.*}"

## @memberof    libsct::Script
declare -g  varsct_clr_green='\e[32m'
## @memberof    libsct::Script
declare -g  varsct_clr_cyan='\e[96m'

## @memberof    libsct::Script
declare -g  varsct_log_file=''

# List of commands supported.
## @memberof    libsct::Script
declare -gA varsct_Script_option_command_lst=()
## @memberof    libsct::Script
declare -g  varsct_Script_option_command_def=''

# List of supported options:
# -d : Run all commands in debug mode
# -f : Run following commands even on error.
# -l : Send command output to the log file.
# -v : Run all commands in verbose mode
# -x : Run all commands in bash xtrace Mode
## @memberof  libsct::option
declare -gA varsct_opt_name_lst
varsct_opt_name_lst['d']='debug'
varsct_opt_name_lst['f']='force'
varsct_opt_name_lst['l']='logfile'
varsct_opt_name_lst['v']='verbose'
varsct_opt_name_lst['x']='xtrace'
## @memberof    libsct
declare -gA varsct_opt_set_lst=()

declare -ga varsct_stp_task_lst=()

# Global running command/step/task variables
declare -g  varsct_run_command
declare -ga varsct_run_command_argv
declare -ga varsct_run_command_lst
declare -g  varsct_run_step
declare -ga varsct_run_step_lst
declare -g  varsct_run_task
declare -ga varsct_run_task_lst

## @fn libsct::Script::__init__()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
# shellcheck disable=SC2034
function libsct::Script::__init__() {
  local -i retval=0
  varsct_env_prefix="${1:-SCRIPT_SCT}"
  call self.env_check "${varsct_env_prefix}_PRODUCT" || return 1
  # Expand variables content
  libglb::Environment::expand "${varsct_env_prefix}_*"
  # Declare script global variables varsct_*
  local log_ind="${varsct_env_prefix}_LOGLEVEL"
  declare -gi varsct_log_ind="${!log_ind:-0}"
  # Normalize paths and create required directories
  local outpath="${varsct_env_prefix}_OUTPATH"
  local logpath="${varsct_env_prefix}_LOGPATH"
  local logext="${varsct_env_prefix}_LOGEXT"
  for i in "${outpath}" "${logpath}"; do
    if [[ ${!i:0:1} == '/' ]]; then eval "${i}=${!i%/}/"
    else eval "${i}=${varsct_sct_pwd}${!i%/}/"; fi
    if [[ ! -e "${!i}" ]]; then
      mkdir -p "${!i}" || retval=${?}
      [[ ${retval} -ne 0 ]] && \
        call self.display_error "Unable to create directory: ${!i}"
    fi
  done
  [[ ${retval} -ne 0 ]] && return ${retval}
  # Clean previous log files
  rm -f "${logpath}"*".${!logext}" || retval=${?}
  if [[ ${retval} -ne 0 ]]; then
    local msg="Unable to remove previous log files:"
    call self.display_error "${msg} \n${logpath}*.${!logext}"
    return ${retval}
  fi
  return ${retval}
}


declare -gA libsct_Script_steps

## @fn libsct::Script::set_step()
## @brief       Add a new or replace an existing step into the script.
## @param $1    Step name in lower case, as it will be used on the command line.
## @param $2    Step class name, including namespace.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
function libsct::Script::set_step() {
  local stp_name="${1}"
  local stp_class="${2}"
  # shellcheck disable=SC2034
  libsct_Script_steps[${stp_name}]="${stp_class}"
}

## @fn libsct::Script::set_command()
## @brief       Add a new or replace an existing command into the script.
## @param $1    Command option as it will be used on the command line.
## @param $2    Command name corresponding to steps method.
## @param ...   Ordered list of steps implementing this command method.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
function libsct::Script::set_command() {
  local cmd_opt="${1//-/}"
  local cmd_name="${2}"
  if [[ -z "${varsct_Script_option_command_def}" ]]; then
    varsct_Script_option_command_def="${cmd_opt}"
  fi
  varsct_Script_option_command_lst[${cmd_opt}]="${cmd_name}"
  # Legacy support
  eval "declare -ga varsct_cmd_${cmd_name}_lst"
  eval "varsct_cmd_${cmd_name}_lst=(${*:3})"
}

## @fn libsct::Script::_display_msg()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
function libsct::Script::_display_msg() {
  local dsp_msg
  dsp_msg=$(printf "[%s] %${varsct_log_ind}s%s\n" "${1}" '' "${2}")
  # First log to file if supported
  [[ -n "${varsct_log_file}" ]] && \
    echo -e "\n${dsp_msg}" >> "${varsct_log_file}"
  # Terminal color if supported
  if [[ -z "${NO_COLOR}" ]]; then
    dsp_msg="${__display_color__}${dsp_msg}\e[39m\e[49m"
  elif [[ -z "${varsct_log_file}" ]]; then
    dsp_msg="\n${dsp_msg}"
  fi
  # Send the message to stdout(fd 3 if there is a logfile, fd 1 otherwise)
  if [[ -n "${varsct_log_file}" ]]; then echo -e "${dsp_msg}" >&3;
  else echo -e "${dsp_msg}" >&1; fi
}
## @fn libsct::Script::display_start()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
function libsct::Script::display_start() {
  local __display_color__="\e[32m"  # green
  libsct::Script::_display_msg "START" "${@}"
}
## @fn libsct::Script::display_info()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
function libsct::Script::display_info() {
  local __display_color__="\e[96m"  # cyan
  libsct::Script::_display_msg "INFO " "${@}"
}
## @fn libsct::Script::display_ok()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
function libsct::Script::display_ok() {
  local __display_color__="\e[32m"  # green
  libsct::Script::_display_msg "OK   " "${@}"
}
## @fn libsct::Script::display_error()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
function libsct::Script::display_error() {
  local __display_color__="\e[31m"  # red
  libsct::Script::_display_msg "ERROR" "${@}"
}

## @fn libsct::Script::dump()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
function libsct::Script::dump() {
  local -i retval=0
  local    dump_head dump_env
  dump_head=$(call self.dump_header "${@}")  || retval=${?}
  dump_env=$(call self.dump_env)             || retval=${?}
  if [[ "${dump_env}" ]]; then
    dump_sep="#$(printf '\x2d%.0s' {1..79})"
    dump_env+='\n'
    dump_env+=$(libglb::Array::colorize "${varsct_clr_green}" "${dump_sep}")
  fi
  echo -e "${dump_head}\n${dump_env}"
  return ${retval}
}

## @fn libsct::Script::dump_header()
## @memberof    libsct::Script
function libsct::Script::dump_header() {
  local dump_sep
  dump_sep="#$(printf '\x2d%.0s' {1..79})"
  local -a dump_lst=(
    "${dump_sep}"
    "# ${USER}@$(hostname) $(date)"
    "# ${varsct_sct_path}${varsct_sct_name}.${varsct_sct_ext} ${*}"
    "${dump_sep}"
  )
  libglb::Array::colorize "${varsct_clr_green}" "${dump_lst[@]}"
}

## @fn libsct::Script::dump_env()
## @memberof    libsct::Script
function libsct::Script::dump_env() {
  local -i retval=0
  local    dump_env=''
  local -a dump_lst dump_tmp
  # Script environment variables
  local dump_prd="${varsct_env_prefix}_PRODUCT"
  dump_prd="${!dump_prd^^}_*"
  mapfile -t dump_lst < <(libglb::Environment::dump \
    "${varsct_env_prefix}_*" "${dump_prd}")
  if [[ ${#dump_lst[@]} -gt 0 ]]; then
    dump_tmp=('#' '# Script environment variables :' '#')
    dump_env="${dump_env:+${dump_env}\n}"
    dump_env+=$(libglb::Array::colorize "${varsct_clr_green}" "${dump_tmp[@]}")
    dump_env+='\n'
    dump_env+=$(libglb::Array::colorize "${varsct_clr_cyan}"  "${dump_lst[@]}")
  fi
  # Product environment variables
  dump_lst=()
  [[ ${dump_prd} ]] && \
    mapfile -t dump_lst < <(libglb::Environment::dump \
      "${dump_prd}" "${varsct_env_prefix}_*")
  if [[ ${#dump_lst[@]} -gt 0 ]]; then
    dump_tmp=('#' '# Product environment variables :' '#')
    dump_env="${dump_env:+${dump_env}\n}"
    dump_env+=$(libglb::Array::colorize "${varsct_clr_green}" "${dump_tmp[@]}")
    dump_env+='\n'
    dump_env+=$(libglb::Array::colorize "${varsct_clr_cyan}"  "${dump_lst[@]}")
  fi
  [[ -n "${dump_env}" ]] && printf '%s' "${dump_env}"
  return ${retval}
}

# Check all required variables are defined in the environment
## @fn libsct::Script::env_check()
## @memberof    libsct::Script
function libsct::Script::env_check() {
  local -i  retval=0
  local     msg
  for i in "${@}"; do
    [[ "${!i}" ]] && continue
    msg="The variable ${i} must be defined in the environment"
    call self.display_error "${msg}"
    retval=1
  done
  return ${retval}
}

## @fn libsct::Script::logfile_create()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
function libsct::Script::logfile_create() {
  local -i retval=0
  local log_tmp log_ext
  if [[ -n "${1}" ]]; then 
    varsct_log_file="${1}"
  else
    log_tmp="${varsct_env_prefix}_LOGPATH"
    log_ext="${varsct_env_prefix}_LOGEXT"
    varsct_log_file="${!log_tmp}${varsct_sct_name}.${!log_ext}"
  fi
  [[ -e "${varsct_log_file}" ]] && return
  log_tmp=$(NO_COLOR="y" \
    call self.dump_header "${varsct_run_argv[@]}") || retval=${?}
  log_ext=$(NO_COLOR="y" call self.logfile_env)    || retval=${?}
  [[ -n "${log_ext}" ]] && log_ext+="\n#$(printf '\x2d%.0s' {1..79})"
  echo -e "${log_tmp}\n${log_ext}" > "${varsct_log_file}"    || retval=${?}
  return ${retval}
}

## @fn libsct::Script::logfile_env()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
function libsct::Script::logfile_env() {
  local -i retval=0
  local -a log_lst log_tmp
  local    log_env=''
  # Environment variables (Debug Mode)
  if [[ "${varsct_opt_set_lst['debug']}" ]]; then
    log_lst=()
    for i in {A..Z}; do
      mapfile -t log_tmp < <(libglb::Environment::dump "${i}*")
      log_lst+=("${log_tmp[@]}")
    done
    if [[ ${#log_lst[@]} -gt 0 ]]; then
      log_tmp=('#' '# Environment variables (Debug Mode) :' '#')
      log_env+=$(printf '%s\n' "${log_tmp[@]}")
      log_env+='\n'
      log_env+=$(printf '%s\n' "${log_lst[@]}")
    fi
  # Environment variables (Normal Mode)
  else
    log_env="$(NO_COLOR='y' call self.dump_env)"
  fi
  [[ "${log_env}" ]] && printf '%s' "${log_env}"
  return ${retval}
}

## @fn libsct::Script::parse_args()
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
function libsct::Script::parse_args() {
  local -i retval=0
  eval "varsct_run_argv=(\${${varsct_env_prefix}_CMDARGS[@]})"
  varsct_run_argv+=("${@}")

  # Parse command line options
  local opt_get opt_char cmd_prev="${varsct_Script_option_command_def}"
  opt_get=$(printf '%s' "${!varsct_opt_name_lst[@]}")
  opt_get+=$(printf '%s:' "${!varsct_Script_option_command_lst[@]}")
  while true ; do
    getopts ":${opt_get}" opt_char "${varsct_run_argv[@]}"
    # Check if opt_char is a known option (flag)
    local opt_name="${varsct_opt_name_lst[${opt_char}]}"
    if [[ -n "${opt_name}" ]]; then
      varsct_opt_set_lst["${opt_name}"]="-${opt_char}"
      continue
    fi
    # If opt_char is an unknown option, reuse the previous command
    if [[ "${opt_char}" == '?' ]]; then
      opt_char="${cmd_prev}"
      OPTARG="${varsct_run_argv[$(( OPTIND - 1 ))]}"
      (( OPTIND ++ ))
    # Command without parameter (step)
    elif [[ "${opt_char}" == ':' ]]; then
      opt_char="${OPTARG}"
      OPTARG=""
      (( OPTIND ++ ))
    fi
    # Check if opt_char is a known command
    opt_name="${varsct_Script_option_command_lst[${opt_char}]}"
    if [[ -z "${opt_name}" ]]; then
      # Unsupported option or command
      call self.display_error "Illegal option : -${opt_char}"
      retval=1
    else
      local -a  cmd_step_lst
      local     cmd_range="${OPTARG}"
      eval "cmd_step_lst=(\"\${varsct_cmd_${opt_name}_lst[@]}\")"
      # If ${OPTARG} is empty, use the default command step
      [[ -z "${cmd_range}" ]] && \
        cmd_range="${cmd_step_lst[*]: -1}"
      # If ${OPTARG} is not a range, create the range with the first
      # dependency
      [[ "${cmd_range##*:*}" ]] && \
        cmd_range="${cmd_step_lst[0]}:${cmd_range}"
      varsct_run_command_lst+=("${opt_name}")
      mapfile -t cmd_step_lst < <(libglb::Array::slice_byvalue \
        "${cmd_range}" "${cmd_step_lst[@]}")
      varsct_run_command_argv+=("${cmd_step_lst[*]}")
      cmd_prev="${opt_char}"
      # Check command arguments (steps)
      if [[ ${#cmd_step_lst[@]} -eq 0 ]]; then
        call self.display_error "Unknown step to ${opt_name} : ${OPTARG}"
        retval=1
      fi
    fi
    [[ ${OPTIND} -gt ${#varsct_run_argv[@]} ]] && break
  done
  if [[ ${retval} -ne 0 ]]; then
    # Display help
    :
  fi
  return ${retval}
}

## @fn libsct::Script::run()
## @brief       Script main method, the real entry point.
## @details     Run all specified commands in given order.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed command.
## @memberof    libsct::Script
function libsct::Script::run() {
  # Defining script as a keyword on the stack like self or super, so it can be
  # used by all called methods even those of other objects.
  # shellcheck disable=SC2034
  local script="${self}"
  local outpath="${varsct_env_prefix}_OUTPATH"
  # Print script header and normalized environment
  call self.dump "${@}" || return ${?}
  # Create a bash environment file
  (NO_COLOR="y" call self.dump "${@}") > \
    "${!outpath}${varsct_sct_name}.env" || return ${?}
  # Check requested environment variable
  call self.env_check "${varsct_env_lst[@]}" || return ${?}
  # Parse command line options and commands
  call self.parse_args "${@}" || return ${?}

  # Execute all commands
  local -i  cmd_retval=0 retval=0
  local     cmd_name cmd_argv
  # Setup shell options and file descriptors
  shopt -s extglob  # Required for pattern matching expansion
  exec 3>&1 4>&2    # Save file descriptors
  for cmd_ndx in "${!varsct_run_command_lst[@]}"; do
    cmd_name="${varsct_run_command_lst[cmd_ndx]}"
    cmd_argv="${varsct_run_command_argv[cmd_ndx]}"
    call self.display_start "${cmd_name}(${cmd_argv[*]})"
    (( varsct_log_ind += 2 ))
    eval "call self.run_command ${cmd_name} ${cmd_argv[*]}" || cmd_retval=${?}
    (( varsct_log_ind -= 2 ))
    if [[ ${cmd_retval} -eq 0 ]]; then
      call self.display_ok "${cmd_name}() returned ${cmd_retval}"
    else
      call self.display_error "${cmd_name}() returned ${cmd_retval}"
      # When force -f option is used, do not stop on the first error and
      # do not override previous error code.
      # Force is usefull when clean is used after build.
      [[ ${retval} -eq 0 ]] && retval=${cmd_retval}
      [[ ! "${varsct_opt_set_lst['force']}" ]] && break
      cmd_retval=0
    fi
  done
  # Restore file descriptors and shell options
  exec 1>&3 3>&- 2>&4 4>&-  # Restore file descriptors
  shopt -u extglob
  return ${retval}
}

## @fn libsct::Script::run_command()
## @brief       Run a command on all specified steps.
## @retval 0    If the method succeeded.
## @retval $?   Error code of the first failed step.
## @memberof    libsct::Script
function libsct::Script::run_command() {
  local -i retval=0
  local run_logpath run_logurl
  local run_class run_function run_display
  local run_xtrace
  run_xtrace=$(shopt -po xtrace)

  # Global running command/step/task variables
  varsct_run_command="${1}"
  varsct_run_step_lst=("${@:2}")
  for varsct_run_step in "${varsct_run_step_lst[@]}"; do
    # Skip class if the command method does not exist.
    run_class="${libsct_Script_steps[${varsct_run_step}]}"
    run_function="${run_class}::${varsct_run_command}"
    [[ $(type -t "${run_function}") != 'function' ]] && continue

    # Redirect stdout and stderr file descriptors to the log file
    if [[ "${varsct_opt_set_lst['logfile']}" ]]; then
      call self.logfile_create "${varsct_log_file}" || retval=${?}
      [[ ${retval} -ne 0 ]] && break
    fi
    if [[ -z "${varsct_log_file}" ]]; then
      run_logurl=''
    else
      run_logpath="${varsct_env_prefix}_LOGPATH"
      run_logurl="${varsct_env_prefix}_RUNURL"
      run_logurl="${!run_logurl}${varsct_log_file#${!run_logpath}}"
      exec >> "${varsct_log_file}" 2>&1
    fi
    if [[ ! "${varsct_opt_set_lst['xtrace']}" ]]; then set +x
    else set "${varsct_opt_set_lst['xtrace']}"; fi

    # shellcheck disable=SC2034
    declare -ga varsct_stp_task_lst=()
    varsct_run_task_lst=()
    varsct_run_task=''
    run_display="${run_class##*::}"
    call self.display_info "${run_display}: Initializing ..."
    (( varsct_log_ind += 2 ))
    # Temporary patch, will be replaced by new
    call "${run_class}.__init__" "${varsct_run_command}" || retval=${?}
    (( varsct_log_ind -= 2 ))
    # Call the command function
    run_display+=".${varsct_run_command}()"
    if [[ ${retval} -eq 0 ]]; then
      call self.display_start "${run_display}: Running ..."
      (( varsct_log_ind += 2 ))
      call "${run_class}.${varsct_run_command}" || retval=${?}
      (( varsct_log_ind -= 2 ))
    fi
    # Temporary patch, will be replaced by del
    call "${run_class}.__del__" || retval=${?}
    run_display+="${run_logurl:+: ${run_logurl}}"
    if [[ ${retval} -eq 0 ]]; then
      call self.display_ok "${run_display}"
    else
      call self.display_error "${run_display}"
    fi

    # Restore stdout and stderr file descriptors
    ${run_xtrace}
    [[ -n "${varsct_log_file}" ]] && exec 1>&3 2>&4
    [[ ${retval} -ne 0 ]] && break
  done

  varsct_log_file=""
  return ${retval}
}

# BBOSS Copyright (C) 2019, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later
# https://gitlab.com/bboss-org/bboss-scripts

require 'simplecov'
SimpleCov.start do
  if ENV['BUILD_SCT_RUNINDEX'] and not ENV['BUILD_SCT_RUNINDEX'].empty? then
    coverage_dir 
      "#{ENV['BUILD_SCT_OUTPATH']}coverage~#{ENV['BUILD_SCT_RUNINDEX']}"
  end
  add_filter %r{^/\..*/}
  add_filter "/tools/bash_unit"

  add_group 'Build',    "build.sh"
  add_group 'Install',  "install.sh"
  add_group 'Lib',      '/lib/'
  add_group 'Tests',    '/tests/'
  add_group 'Tools',    '/tools/'
end

# Upload code coverage report to https://codecov.io if the token is defined in
# the environment, otherwise create a local coverage report inside ./coverage
if ENV['CODECOV_TOKEN'] and not ENV['CODECOV_TOKEN'].empty? then
  require 'codecov'
  SimpleCov.formatter = SimpleCov::Formatter::Codecov
end


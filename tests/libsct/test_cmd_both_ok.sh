#!/usr/bin/env bash

# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck source=../libsct/test_cmd.sh
source "$(dirname "${BASH_SOURCE[0]}")/../libsct/test_cmd.sh"

#+----------------------------------------------------------------------------
#                         Install step class
#-----------------------------------------------------------------------------
class libtst::steps::Install libsct::Step
{
  method    __init__
  method    build
  method    clean
}
function libtst::steps::Install::__init__() {
  libtst_chk_display_hook; }
function libtst::steps::Install::build() {
  libtst_chk_display_hook; }
function libtst::steps::Install::clean() {
  libtst_chk_display_hook; }

call libtst::Script.set_step "install"  "libtst::steps::Install"

#-----------------------------------------------------------------------------
#                        bash_unit declarations
#-----------------------------------------------------------------------------
function setup_suite() {
  libtst_tst_setup_suite
}

function setup() {
  libtst_tst_setup
}

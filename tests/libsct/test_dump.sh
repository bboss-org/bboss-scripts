#!/usr/bin/env bash

# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck source=../lib/libsct.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libsct.sh"

#-----------------------------------------------------------------------------
#                       tstsct_dump functions
#-----------------------------------------------------------------------------
function todo_test_dump_01_header() {
  array_txt="$(call libsct::Script.dump_header)"
  echo -e "$array_txt"

  array_txt="$(NO_COLOR='y' call libsct::Script.dump_header)"
  echo -e "$array_txt"
}

function todo_test_dump_02_env() {

  export INSTALL_SCT_PRODUCT="MyTest"
  export MYTEST_INFO='info'

#  varsct_opt_set_lst['debug']='-d'
  array_txt="$(libsct::Script.dump_env)"
  [[ "${array_txt}" ]] && echo -e "${array_txt}"

  array_txt="$(NO_COLOR='y' libsct::Script.dump_env)"
  [[ "${array_txt}" ]] && echo -e "${array_txt}"

  return 0
}

function todo_test_dump_03() {

  export INSTALL_SCT_PRODUCT="MyTest"
  export MYTEST_INFO='info'

#  varsct_opt_set_lst['debug']='-d'
  array_txt="$(libsct::Script.dump)"
  [[ "${array_txt}" ]] && echo -e "${array_txt}"

  array_txt="$(NO_COLOR='y' libsct::Script.dump)"
  [[ "${array_txt}" ]] && echo -e "${array_txt}"

  return 0
}


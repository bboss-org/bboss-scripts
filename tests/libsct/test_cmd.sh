#!/usr/bin/env bash

# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck source=../lib/libbld.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libsct.sh"
# shellcheck source=../lib/libtst.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libtst.sh"

#+----------------------------------------------------------------------------
#                         libtst::Script class
#-----------------------------------------------------------------------------
export SCRIPT_SCT_PRODUCT="test"

class libtst::Script libsct::Script

# Define default steps for python builder script
call libtst::Script.set_step "linter"   "libtst::steps::Linter"
call libtst::Script.set_step "test"     "libtst::steps::Test"
# Intentionally empty, implemented in test_cmd_*
call libtst::Script.set_step "install"  "libsct::Step"
call libtst::Script.set_step "package"  "libtst::steps::Package"
call libtst::Script.set_step "deploy"   "libtst::steps::Deploy"

# Define default commands for python builder script
call libtst::Script.set_command -b "build" \
  "linter" "test" "install" "package" "deploy"
call libtst::Script.set_command -c "clean" \
  "deploy" "package" "install" "test" "linter"

#+----------------------------------------------------------------------------
#                    libtst::steps::Linter class
#-----------------------------------------------------------------------------
# Class without __init__
class libtst::steps::Linter libsct::Step
{
  method    build
  method    clean
}
function libtst::steps::Linter::build() {
  libtst_chk_display_hook; }
function libtst::steps::Linter::clean() {
  libtst_chk_display_hook; }

#+----------------------------------------------------------------------------
#                    libtst::steps::Test class
#-----------------------------------------------------------------------------
# Class with all commands
class libtst::steps::Test libsct::Step
{
  method    __init__
  method    build
  method    clean
}
function libtst::steps::Test::__init__() {
  libtst_chk_display_hook; }
function libtst::steps::Test::build() {
  libtst_chk_display_hook; }
function libtst::steps::Test::clean() {
  libtst_chk_display_hook; }

#+----------------------------------------------------------------------------
#                  libtst::steps::Install class
#-----------------------------------------------------------------------------
# Class without any commands, implemented in test_cmd_*

#+----------------------------------------------------------------------------
#                  libtst::steps::Package class
#-----------------------------------------------------------------------------
# Class with all commands
class libtst::steps::Package libsct::Step
{
  method    __init__
  method    build
  method    clean
}
function libtst::steps::Package::__init__() {
  libtst_chk_display_hook; }
function libtst::steps::Package::build() {
  libtst_chk_display_hook; }
function libtst::steps::Package::clean() {
  libtst_chk_display_hook; }

#+----------------------------------------------------------------------------
#                  libtst::steps::Deploy class
#-----------------------------------------------------------------------------
# Class without clean command
class libtst::steps::Deploy libsct::Step
{
  method    __init__
  method    build
}
function libtst::steps::Deploy::__init__() {
  libtst_chk_display_hook; }
function libtst::steps::Deploy::build() {
  libtst_chk_display_hook; }

#-----------------------------------------------------------------------------
#                        bash_unit declarations
#-----------------------------------------------------------------------------
function setup_suite()  { libtst_tst_setup_suite; }
function setup()        { libtst_tst_setup; }

#-----------------------------------------------------------------------------
#                     test_cmd_build functions
#-----------------------------------------------------------------------------
function _test_cmd_build() {
  libtst_run_main 'libtst' "${*}"
  local -i cmd_res=${vartst_run_main}

  libtst_chk_display_line_cmd 'build' 'linter:deploy'
  local -i dsp_res=${vartst_chk_display_cmd}

  libtst_chk_display_res 'build' '-1'
  local -i cmd_exp=${vartst_chk_display_cmd}

  assert_equals "${cmd_exp}" "${cmd_res}" \
    "call libsct::Script.run returned ${cmd_res} instead than ${cmd_exp}"
}

# test_cmd_build with different command line syntaxes
function test_cmd_build_01() { _test_cmd_build "-b deploy"; }
function test_cmd_build_02() { _test_cmd_build "-b linter:deploy"; }
function test_cmd_build_03() { _test_cmd_build "deploy"; }
function test_cmd_build_04() { _test_cmd_build "linter:deploy"; }

function _test_cmd_build_clean() {
  libtst_run_main 'libsct' "${*}"
  local -i cmd_res=${vartst_run_main}

  libtst_chk_display_line_cmd 'build' 'linter:deploy'
  local -i dsp_res=${vartst_chk_display_cmd}

  if [[ ${dsp_res} -eq 1 ]]; then
    libtst_chk_display_res 'build' '-1'
  else
    libtst_chk_display_line_cmd 'clean' 'deploy:linter'
    libtst_chk_display_res 'clean' '-1'
  fi
  [[ ${dsp_res} -eq 0 ]] && dsp_res=${vartst_chk_display_cmd}

  assert_equals "${dsp_res}" "${cmd_res}" \
    "call libsct::Script.run returned ${cmd_res} instead than ${dsp_res}"
}

# test_cmd_build_clean with different command line syntaxes
function test_cmd_build_clean_01() {
  _test_cmd_build_clean "-b deploy -c linter"; }
function test_cmd_build_clean_02() {
  _test_cmd_build_clean "-b linter:deploy -c deploy:linter"; }
function test_cmd_build_clean_03() {
  _test_cmd_build_clean "deploy -c linter"; }
function test_cmd_build_clean_04() {
  _test_cmd_build_clean "linter:deploy -c deploy:linter"; }

function _test_cmd_build_twice() {
  libtst_run_main libsct "${*}"
  local -i cmd_res=${vartst_run_main}

  libtst_chk_display_line_cmd 'build' 'linter:deploy'
  local -i dsp_res=${vartst_chk_display_cmd}

  if [[ ${dsp_res} -ne 1 ]]; then
    libtst_chk_display_line_cmd 'build' 'linter:deploy'
  fi

  libtst_chk_display_res 'build' '-1'
  [[ ${dsp_res} -eq 0 ]] && dsp_res=${vartst_chk_display_cmd}

  assert_equals "${dsp_res}" "${cmd_res}" \
    "call libsct::Script.run returned ${cmd_res} instead than ${dsp_res}"
}

# test_cmd_build_twice with different command line syntaxes
function test_cmd_build_twice_01() {
  _test_cmd_build_twice "-b deploy -b deploy"; }
function test_cmd_build_twice_02() {
  _test_cmd_build_twice "-b linter:deploy -b linter:deploy"; }
function test_cmd_build_twice_03() {
  _test_cmd_build_twice "-b deploy deploy"; }
function test_cmd_build_twice_04() {
  _test_cmd_build_twice "-b linter:deploy linter:deploy"; }
function test_cmd_build_twice_05() {
  _test_cmd_build_twice "deploy deploy"; }
function test_cmd_build_twice_06() {
  _test_cmd_build_twice "linter:deploy linter:deploy"; }

#-----------------------------------------------------------------------------
#                     test_cmd_clean functions
#-----------------------------------------------------------------------------
function _test_cmd_clean() {
  libtst_run_main 'libsct' "${*}"
  local -i cmd_res=${vartst_run_main}

  libtst_chk_display_line_cmd 'clean' 'deploy:linter'
  local -i dsp_res=${vartst_chk_display_cmd}

  libtst_chk_display_res 'clean' '-1'
  local -i cmd_exp=${vartst_chk_display_cmd}

  assert_equals "${cmd_exp}" "${cmd_res}" \
    "call libsct::Script.run returned ${cmd_res} instead than ${cmd_exp}"
}

# test_cmd_clean with different command line syntaxes
function test_cmd_clean_01() { _test_cmd_clean "-c linter"; }
function test_cmd_clean_02() { _test_cmd_clean "-c deploy:linter"; }

function _test_cmd_clean_build() {
  libtst_run_main 'libsct' "${*}"
  local -i cmd_res=${vartst_run_main}

  libtst_chk_display_line_cmd 'clean' 'deploy:linter'
  local -i dsp_res=${vartst_chk_display_cmd}

  if [[ ${dsp_res} -eq 1 ]]; then
    libtst_chk_display_res 'clean' '-1'
  else
    libtst_chk_display_line_cmd 'build' 'linter:deploy'
    libtst_chk_display_res 'build' '-1'
  fi
  [[ ${dsp_res} -eq 0 ]] && dsp_res=${vartst_chk_display_cmd}

  assert_equals "${dsp_res}" "${cmd_res}" \
    "call libsct::Script.run returned ${cmd_res} instead than ${dsp_res}"
}

# test_cmd_build_clean with different command line syntaxes
function test_cmd_clean_build_01() {
  _test_cmd_clean_build "-c linter -b deploy"; }
function test_cmd_clean_build_02() {
  _test_cmd_clean_build "-c deploy:linter -b linter:deploy"; }

function _test_cmd_clean_twice() {
  libtst_run_main libsct "${*}"
  local -i cmd_res=${vartst_run_main}

  libtst_chk_display_line_cmd 'clean' 'deploy:linter'
  local -i dsp_res=${vartst_chk_display_cmd}

  if [[ ${dsp_res} -ne 1 ]]; then
    libtst_chk_display_line_cmd 'clean' 'deploy:linter'
  fi

  libtst_chk_display_res 'clean' '-1'
  [[ ${dsp_res} -eq 0 ]] && dsp_res=${vartst_chk_display_cmd}

  assert_equals "${dsp_res}" "${cmd_res}" \
    "call libsct::Script.run returned ${cmd_res} instead than ${dsp_res}"
}

# test_cmd_clean_twice with different command line syntaxes
function test_cmd_clean_twice_01() {
  _test_cmd_clean_twice "-c linter -c linter"; }
function test_cmd_clean_twice_02() {
  _test_cmd_clean_twice "-c deploy:linter -c deploy:linter"; }
function test_cmd_clean_twice_03() {
  _test_cmd_clean_twice "-c linter linter"; }
function test_cmd_clean_twice_04() {
  _test_cmd_clean_twice "-c deploy:linter deploy:linter"; }

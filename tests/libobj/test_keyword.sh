#!/usr/bin/env bash

# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck source=../lib/libglb.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libobj.sh"

#-----------------------------------------------------------------------------
#                       libobj keyword class
#-----------------------------------------------------------------------------
# shellcheck disable=SC2154
function test_keyword_class() {
  # Successful class definition
  class ClassA
  assert_equals "0" "${?}" "class ClassA"
  assert_equals "libobj::Object" "${ClassA[__bases__]}"

  class ClassB
  assert_equals "0" "${?}" "class ClassB"
  assert_equals "libobj::Object" "${ClassB[__bases__]}"

  class ClassC ClassA
  assert_equals "0" "${?}" "class ClassC ClassA"
  assert_equals "ClassA" "${ClassC[__bases__]}"

  class ClassD ClassA ClassB
  assert_equals "0" "${?}" "class ClassD ClassA ClassB"
  assert_equals "ClassA ClassB" "${ClassD[__bases__]}"

  # Unsuccessful class definition
  assert_fail "class ClassA"
  assert_fail "class ClassF ClassE"
}

function test_keyword_class_debug() {
  local argv msg_dbg msg_exp msg_res
  local -a msg_chk
  msg_dbg="./../lib/libobj.sh: class(): "

  argv="classb"
  msg_exp="${msg_dbg}Class name must start with an uppercase letter!"
  msg_res="$(class ${argv} 2>&1)"
  assert_equals "255" "${?}" "class ${argv}"
  mapfile -t msg_chk < <(echo "${msg_res}")
  assert_equals "${msg_exp}" "${msg_chk[0]}"

  argv="Class-b"
  msg_exp="${msg_dbg}Class name must contain only alphabetical characters"
  msg_exp+=" or numbers!"
  msg_res="$(class ${argv} 2>&1)"
  assert_equals "255" "${?}" "class ${argv}"
  mapfile -t msg_chk < <(echo "${msg_res}")
  assert_equals "${msg_exp}" "${msg_chk[0]}"

  class ClassA
  argv="ClassA"
  msg_exp="${msg_dbg}The class ClassA already exists!"
  msg_res="$(class ${argv} 2>&1)"
  assert_equals "255" "${?}" "class ${argv}"
  mapfile -t msg_chk < <(echo "${msg_res}")
  assert_equals "${msg_exp}" "${msg_chk[0]}"

  argv="ClassC ClassB"
  msg_exp="${msg_dbg}Unknown base class: ClassB"
  msg_res="$(class ClassC ClassB 2>&1)"
  assert_equals "255" "${?}" "class ${argv}"
  mapfile -t msg_chk < <(echo "${msg_res}")
  assert_equals "${msg_exp}" "${msg_chk[0]}"
}

function test_keyword_class_name() {
  # Check good class names
  assert "class 'ClassA'"
  assert "class 'Class01'"
  assert "class 'tstobj::ClassA'"
  assert "class 'tstobj::Class01'"

  # Class name must start with an uppercase letter!
  assert_fail "class 'classA'"
  assert_fail "class '01class'"
  assert_fail "class 'tstobj::classA'"
  assert_fail "class 'tstobj::01class'"

  # Class name must only contains alphabetical or number!
  assert_fail "class 'Class-A'"
  assert_fail "class 'Class_01'"
  assert_fail "class 'tstobj::class-A'"
  assert_fail "class 'tstobj::class_01'"
}

#-----------------------------------------------------------------------------
#             libobj keyword method & property helpers
#-----------------------------------------------------------------------------
## @fn _keyword_method()
## @param $1    Property type `property` or `method`
## @param $2    Property name prefix : prop or meth
function _keyword_method() {
  local -a list_exp 
  local -a list_res

  # Successful property definition
  class ClassA
  {
    "${1}"    "${2}_A01"
  }
  assert_equals "${1}" "${ClassA[${2}_A01]}"
  list_exp=("__bases__" "${2}_A01")
  mapfile -t list_res < <(printf '%s\n' "${!ClassA[@]}" | sort)
  assert_equals "${list_exp[*]}" "${list_res[*]}"

  # A property is already defined with this name!
  class ClassB
  {
    property  "${2}_B01"
    assert_fail "${1} ${2}_B01"
  }
  # A method is already defined with this name!
  class ClassC
  {
    method    "${2}_C01"
    assert_fail "${1} ${2}_C01"
  }
}

function _keyword_method_debug() {
  local argv msg_dbg msg_exp msg_res
  local -a msg_chk
  msg_dbg="./../lib/libobj.sh: ${1}(): "

  class ClassA
  {
    method    meth_A01
    property  prop_A01
  }

  argv="01_prop"
  msg_exp="${msg_dbg}The name of a ${1} must start with an alphabetical or "
  msg_exp+="an underline character!"
  msg_res="$(${1} ${argv} 2>&1)"
  assert_equals "255" "${?}" "class ${argv}"
  mapfile -t msg_chk < <(echo "${msg_res}")
  assert_equals "${msg_exp}" "${msg_chk[0]}"

  argv="prop-01"
  msg_exp="${msg_dbg}The name of a ${1} must contain only alphabetical, "
  msg_exp+="number or underline characters!"
  msg_res="$(${1} ${argv} 2>&1)"
  assert_equals "255" "${?}" "class ${argv}"
  mapfile -t msg_chk < <(echo "${msg_res}")
  assert_equals "${msg_exp}" "${msg_chk[0]}"

  argv="meth_A01"
  msg_exp="${msg_dbg}A method is already defined with this name!"
  msg_res="$(${1} ${argv} 2>&1)"
  assert_equals "255" "${?}" "class ${argv}"
  mapfile -t msg_chk < <(echo "${msg_res}")
  assert_equals "${msg_exp}" "${msg_chk[0]}"

  argv="prop_A01"
  msg_exp="${msg_dbg}A property is already defined with this name!"
  msg_res="$(${1} ${argv} 2>&1)"
  assert_equals "255" "${?}" "class ${argv}"
  mapfile -t msg_chk < <(echo "${msg_res}")
  assert_equals "${msg_exp}" "${msg_chk[0]}"
}

#-----------------------------------------------------------------------------
#                       libobj keyword method
#-----------------------------------------------------------------------------
function test_keyword_method() {
  _keyword_method "method" "meth"
}

function test_keyword_method_debug() {
  _keyword_method_debug "method"
}

function test_keyword_method_name() {
  class ClassA
  {
    # Check good method names (Recommended)
    assert "method 'meth'"
    assert "method '_meth'"
    assert "method '__meth__'"
    assert "method 'new_meth'"
    assert "method '_new_meth'"
    assert "method '__new_meth__'"
    assert "method 'new_meth_01'"
    assert "method '_new_meth_01'"
    assert "method '__new_meth_01__'"

    # Check good method names (Supported)
    assert "method 'Meth'"
    assert "method '_Meth'"
    assert "method '__Meth'"

    assert "method 'NewMeth'"
    assert "method '_NewMeth'"
    assert "method '__NewMeth__'"
    assert "method 'NewMeth_01'"
    assert "method '_NewMeth_01'"
    assert "method '__NewMeth_01__'"

    assert "method 'New_Meth'"
    assert "method '_New_Meth'"
    assert "method '__New_Meth__'"
    assert "method 'New_Meth_01'"
    assert "method '_New_Meth_01'"
    assert "method '__New_Meth_01__'"

    assert "method 'METH'"
    assert "method '_METH'"
    assert "method '__METH__'"
    assert "method 'NEW_METH'"
    assert "method '_NEW_METH'"
    assert "method '__NEW_METH__'"
    assert "method 'NEW_METH_01'"
    assert "method '_NEW_METH_01'"
    assert "method '__NEW_METH_01__'"

    # Method name must start with a letter or an underline character!
    assert_fail "method '01_meth'"

    # Method name must only contains alphabetical, number or underline 
    # character!
    assert_fail "method 'meth-A'"
    assert_fail "method 'meth.a'"
    assert_fail "method 'meth::A'"
  }
}

#-----------------------------------------------------------------------------
#                       libobj keyword property
#-----------------------------------------------------------------------------
function test_keyword_property() {
  _keyword_method "property" "prop"
}

function test_keyword_property_debug() {
  _keyword_method_debug "property"
}

function test_keyword_property_name() {
  class ClassA
  {
    # Check good property names (Recommended)
    assert "property 'prop'"
    assert "property '_prop'"
    assert "property '__prop__'"
    assert "property 'new_prop'"
    assert "property '_new_prop'"
    assert "property '__new_prop__'"
    assert "property 'new_prop_01'"
    assert "property '_new_prop_01'"
    assert "property '__new_prop_01__'"

    # Check good property names (Supported)
    assert "property 'Prop'"
    assert "property '_Prop'"
    assert "property '__Prop'"

    assert "property 'NewProp'"
    assert "property '_NewProp'"
    assert "property '__NewProp__'"
    assert "property 'NewProp_01'"
    assert "property '_NewProp_01'"
    assert "property '__NewProp_01__'"

    assert "property 'New_Prop'"
    assert "property '_New_Prop'"
    assert "property '__New_Prop__'"
    assert "property 'New_Prop_01'"
    assert "property '_New_Prop_01'"
    assert "property '__New_Prop_01__'"

    assert "property 'PROP'"
    assert "property '_PROP'"
    assert "property '__PROP__'"
    assert "property 'NEW_PROP'"
    assert "property '_NEW_PROP'"
    assert "property '__NEW_PROP__'"
    assert "property 'NEW_PROP_01'"
    assert "property '_NEW_PROP_01'"
    assert "property '__NEW_PROP_01__'"

    # Property name must start with a letter or an underline character!
    assert_fail "property '01_prop'"

    # Property name must only contains alphabetical, number or underline 
    # character!
    assert_fail "property 'prop-A'"
    assert_fail "property 'prop.a'"
    assert_fail "property 'prop::A'"
  }
}

#-----------------------------------------------------------------------------
#                       libobj keyword new
#-----------------------------------------------------------------------------
function test_keyword_new() {
  :
}

function test_keyword_new_debug() {
  :
}

function test_keyword_new_name() {
  :
}

#-----------------------------------------------------------------------------
#                       libobj keyword call
#-----------------------------------------------------------------------------
function test_keyword_call_class() {
  :
}

function test_keyword_call_object() {
  :
}




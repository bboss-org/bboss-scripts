#!/usr/bin/env bash

# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck source=../lib/libglb.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libobj.sh"


#-----------------------------------------------------------------------------
#                     Test tstcls::ClassA0
#-----------------------------------------------------------------------------
class tstcls::ClassA0
{
  method  call_self
  method  call_self_super
  method  call_super
  method  call_super_self
}
function tstcls::ClassA0::call_self() {
  echo "${FUNCNAME[0]}"
}
function tstcls::ClassA0::call_self_super() {
  echo "${FUNCNAME[0]}"
  call self.call_super
}
function tstcls::ClassA0::call_super() {
  echo "${FUNCNAME[0]}"
}
function tstcls::ClassA0::call_super_self() {
  echo "${FUNCNAME[0]}"
  call self.call_self
}

function test_ClassA0_call_self() {
  local -a result
  mapfile -t result < <( call tstcls::ClassA0.call_self )
  assert_equals "tstcls::ClassA0::call_self" "${result[0]}"
  assert_equals 1 "${#result[@]}"
}

function test_ClassA0_call_self_super() {
  local -a result
  mapfile -t result < <( call tstcls::ClassA0.call_self_super )
  assert_equals "tstcls::ClassA0::call_self_super" "${result[0]}"
  assert_equals "tstcls::ClassA0::call_super" "${result[1]}"
  assert_equals 2 "${#result[@]}"
}

function test_ClassA0_call_super() {
  local -a result
  mapfile -t result < <( call tstcls::ClassA0.call_super )
  assert_equals "tstcls::ClassA0::call_super" "${result[0]}"
  assert_equals 1 "${#result[@]}"
}

function test_ClassA0_call_super_self() {
  local -a result
  mapfile -t result < <( call tstcls::ClassA0.call_super_self )
  assert_equals "tstcls::ClassA0::call_super_self" "${result[0]}"
  assert_equals "tstcls::ClassA0::call_self" "${result[1]}"
  assert_equals 2 "${#result[@]}"
}

#-----------------------------------------------------------------------------
#                     Test tstcls::ClassA1
#-----------------------------------------------------------------------------
class tstcls::ClassA1 tstcls::ClassA0
{
  method  call_self
  method  call_self_super
  method  call_super
  method  call_super_self
}
function tstcls::ClassA1::call_self() {
  echo "${FUNCNAME[0]}"
}
function tstcls::ClassA1::call_self_super() {
  echo "${FUNCNAME[0]}"
  call self.call_super
}
function tstcls::ClassA1::call_super() {
  echo "${FUNCNAME[0]}"
  call super.call_super
}
function tstcls::ClassA1::call_super_self() {
  echo "${FUNCNAME[0]}"
  call super.call_super_self
}

function test_ClassA1_call_self() {
  local -a result
  mapfile -t result < <( call tstcls::ClassA1.call_self )
  assert_equals "tstcls::ClassA1::call_self" "${result[0]}"
  assert_equals 1 "${#result[@]}"
}

function test_ClassA1_call_self_super() {
  local -a result
  mapfile -t result < <( call tstcls::ClassA1.call_self_super )
  assert_equals "tstcls::ClassA1::call_self_super" "${result[0]}"
  assert_equals "tstcls::ClassA1::call_super" "${result[1]}"
  assert_equals "tstcls::ClassA0::call_super" "${result[2]}"
  assert_equals 3 "${#result[@]}"
}

function test_ClassA1_call_super() {
  local -a result
  mapfile -t result < <( call tstcls::ClassA1.call_super )
  assert_equals "tstcls::ClassA1::call_super" "${result[0]}"
  assert_equals "tstcls::ClassA0::call_super" "${result[1]}"
  assert_equals 2 "${#result[@]}"
}

function test_ClassA1_call_super_self() {
  local -a result
  mapfile -t result < <( call tstcls::ClassA1.call_super_self )
  assert_equals "tstcls::ClassA1::call_super_self" "${result[0]}"
  assert_equals "tstcls::ClassA0::call_super_self" "${result[1]}"
  assert_equals "tstcls::ClassA1::call_self" "${result[2]}"
  assert_equals 3 "${#result[@]}"
}

#-----------------------------------------------------------------------------
#                     Test tstcls::ClassA1
#-----------------------------------------------------------------------------
class tstcls::ClassA2 tstcls::ClassA1
{
  method  call_self
  method  call_self_super
  method  call_super
  method  call_super_self
}
function tstcls::ClassA2::call_self() {
  echo "${FUNCNAME[0]}"
}
function tstcls::ClassA2::call_self_super() {
  echo "${FUNCNAME[0]}"
  call self.call_super
}
function tstcls::ClassA2::call_super() {
  echo "${FUNCNAME[0]}"
  call super.call_super
}
function tstcls::ClassA2::call_super_self() {
  echo "${FUNCNAME[0]}"
  call super.call_super_self
}

function test_ClassA2_call_self() {
  local -a result
  mapfile -t result < <( call tstcls::ClassA2.call_self )
  assert_equals "tstcls::ClassA2::call_self" "${result[0]}"
  assert_equals 1 "${#result[@]}"
}

function test_ClassA2_call_self_super() {
  local -a result
  mapfile -t result < <( call tstcls::ClassA2.call_self_super )
  assert_equals "tstcls::ClassA2::call_self_super" "${result[0]}"
  assert_equals "tstcls::ClassA2::call_super" "${result[1]}"
  assert_equals "tstcls::ClassA1::call_super" "${result[2]}"
  assert_equals "tstcls::ClassA0::call_super" "${result[3]}"
  assert_equals 4 "${#result[@]}"
}

function test_ClassA2_call_super() {
  local -a result
  mapfile -t result < <( call tstcls::ClassA2.call_super )
  assert_equals "tstcls::ClassA2::call_super" "${result[0]}"
  assert_equals "tstcls::ClassA1::call_super" "${result[1]}"
  assert_equals "tstcls::ClassA0::call_super" "${result[2]}"
  assert_equals 3 "${#result[@]}"
}

function test_ClassA2_call_super_self() {
  local -a result
  mapfile -t result < <( call tstcls::ClassA2.call_super_self )
  assert_equals "tstcls::ClassA2::call_super_self" "${result[0]}"
  assert_equals "tstcls::ClassA1::call_super_self" "${result[1]}"
  assert_equals "tstcls::ClassA0::call_super_self" "${result[2]}"
  assert_equals "tstcls::ClassA2::call_self" "${result[3]}"
  assert_equals 4 "${#result[@]}"
}

#-----------------------------------------------------------------------------
#                    Test tstcls::ClassB0
#-----------------------------------------------------------------------------
class tstcls::ClassB0
{
  method  call_self
  method  call_self_super
  method  call_super
  method  call_super_self
}
function tstcls::ClassB0::call_self() {
  echo "${FUNCNAME[0]}"
}
function tstcls::ClassB0::call_self_super() {
  echo "${FUNCNAME[0]}"
  call self.call_super
}
function tstcls::ClassB0::call_super() {
  echo "${FUNCNAME[0]}"
  call tstcls::ClassA2.call_super
}
function tstcls::ClassB0::call_super_self {
  echo "${FUNCNAME[0]}"
  call tstcls::ClassA2.call_super_self
}

function test_ClassB0_call_self() {
  local -a result
  mapfile -t result < <( call tstcls::ClassB0.call_self )
  assert_equals "tstcls::ClassB0::call_self"  "${result[0]}"
  assert_equals 1 "${#result[@]}"
}

function test_ClassB0_call_self_super() {
  local -a result
  mapfile -t result < <( call tstcls::ClassB0.call_self_super )
  assert_equals "tstcls::ClassB0::call_self_super" "${result[0]}"
  assert_equals "tstcls::ClassB0::call_super" "${result[1]}"
  assert_equals "tstcls::ClassA2::call_super" "${result[2]}"
  assert_equals "tstcls::ClassA1::call_super" "${result[3]}"
  assert_equals "tstcls::ClassA0::call_super" "${result[4]}"
  assert_equals 5 "${#result[@]}"
}

function test_ClassB0_call_super() {
  local -a result
  mapfile -t result < <( call tstcls::ClassB0.call_super )
  assert_equals "tstcls::ClassB0::call_super" "${result[0]}"
  assert_equals "tstcls::ClassA2::call_super" "${result[1]}"
  assert_equals "tstcls::ClassA1::call_super" "${result[2]}"
  assert_equals "tstcls::ClassA0::call_super" "${result[3]}"
  assert_equals 4 "${#result[@]}"
}

function test_ClassB0_call_super_self() {
  local -a result
  mapfile -t result < <( call tstcls::ClassB0.call_super_self )
  assert_equals "tstcls::ClassB0::call_super_self"  "${result[0]}"
  assert_equals "tstcls::ClassA2::call_super_self"  "${result[1]}"
  assert_equals "tstcls::ClassA1::call_super_self"  "${result[2]}"
  assert_equals "tstcls::ClassA0::call_super_self"  "${result[3]}"
  assert_equals "tstcls::ClassA2::call_self"        "${result[4]}"
  assert_equals 5 "${#result[@]}"
}

#-----------------------------------------------------------------------------
#                    Test tstcls::ClassB1
#-----------------------------------------------------------------------------
class tstcls::ClassB1 tstcls::ClassB0
{
  method  call_self
  method  call_self_super
  method  call_super
  method  call_super_self
}
function tstcls::ClassB1::call_self() {
  echo "${FUNCNAME[0]}"
}
function tstcls::ClassB1::call_self_super() {
  echo "${FUNCNAME[0]}"
  call self.call_super
}
function tstcls::ClassB1::call_super() {
  echo "${FUNCNAME[0]}"
  call super.call_super
}
function tstcls::ClassB1::call_super_self {
  echo "${FUNCNAME[0]}"
  call super.call_super_self
}

function test_ClassB1_call_seff() {
  local -a result
  mapfile -t result < <( call tstcls::ClassB1.call_self )
  assert_equals "tstcls::ClassB1::call_self"  "${result[0]}"
  assert_equals 1 "${#result[@]}"
}

function test_ClassB1_call_self_super() {
  local -a result
  mapfile -t result < <( call tstcls::ClassB1.call_self_super )
  assert_equals "tstcls::ClassB1::call_self_super" "${result[0]}"
  assert_equals "tstcls::ClassB1::call_super" "${result[1]}"
  assert_equals "tstcls::ClassB0::call_super" "${result[2]}"
  assert_equals "tstcls::ClassA2::call_super" "${result[3]}"
  assert_equals "tstcls::ClassA1::call_super" "${result[4]}"
  assert_equals "tstcls::ClassA0::call_super" "${result[5]}"
  assert_equals 6 "${#result[@]}"
}

function test_ClassB1_call_super() {
  local -a result
  mapfile -t result < <( call tstcls::ClassB1.call_super )
  assert_equals "tstcls::ClassB1::call_super" "${result[0]}"
  assert_equals "tstcls::ClassB0::call_super" "${result[1]}"
  assert_equals "tstcls::ClassA2::call_super" "${result[2]}"
  assert_equals "tstcls::ClassA1::call_super" "${result[3]}"
  assert_equals "tstcls::ClassA0::call_super" "${result[4]}"
  assert_equals 5 "${#result[@]}"
}

function test_ClassB1_call_super_self() {
  local -a result
  mapfile -t result < <( call tstcls::ClassB1.call_super_self )
  assert_equals "tstcls::ClassB1::call_super_self" "${result[0]}"
  assert_equals "tstcls::ClassB0::call_super_self" "${result[1]}"
  assert_equals "tstcls::ClassA2::call_super_self" "${result[2]}"
  assert_equals "tstcls::ClassA1::call_super_self" "${result[3]}"
  assert_equals "tstcls::ClassA0::call_super_self" "${result[4]}"
  assert_equals "tstcls::ClassA2::call_self" "${result[5]}"
  assert_equals 6 "${#result[@]}"
}

#-----------------------------------------------------------------------------
#                    Test tstcls::ClassB2
#-----------------------------------------------------------------------------
class tstcls::ClassB2 tstcls::ClassB1
{
  method  call_self
  method  call_self_super
  method  call_super
  method  call_super_self
}
function tstcls::ClassB2::call_self() {
  echo "${FUNCNAME[0]}"
}
function tstcls::ClassB2::call_self_super() {
  echo "${FUNCNAME[0]}"
  call self.call_super
}
function tstcls::ClassB2::call_super() {
  echo "${FUNCNAME[0]}"
  call super.call_super
}
function tstcls::ClassB2::call_super_self() {
  echo "${FUNCNAME[0]}"
  call super.call_super_self
}

function test_ClassB2_call_self() {
  local -a result
  mapfile -t result < <( call tstcls::ClassB2.call_self )
  assert_equals "tstcls::ClassB2::call_self"  "${result[0]}"
  assert_equals 1 "${#result[@]}"
}

function test_ClassB2_call_self_super() {
  local -a result
  mapfile -t result < <( call tstcls::ClassB2.call_self_super )
  assert_equals "tstcls::ClassB2::call_self_super" "${result[0]}"
  assert_equals "tstcls::ClassB2::call_super" "${result[1]}"
  assert_equals "tstcls::ClassB1::call_super" "${result[2]}"
  assert_equals "tstcls::ClassB0::call_super" "${result[3]}"
  assert_equals "tstcls::ClassA2::call_super" "${result[4]}"
  assert_equals "tstcls::ClassA1::call_super" "${result[5]}"
  assert_equals "tstcls::ClassA0::call_super" "${result[6]}"
  assert_equals 7 "${#result[@]}"
}

function test_ClassB2_call_super() {
  local -a result
  mapfile -t result < <( call tstcls::ClassB2.call_super )
  assert_equals "tstcls::ClassB2::call_super" "${result[0]}"
  assert_equals "tstcls::ClassB1::call_super" "${result[1]}"
  assert_equals "tstcls::ClassB0::call_super" "${result[2]}"
  assert_equals "tstcls::ClassA2::call_super" "${result[3]}"
  assert_equals "tstcls::ClassA1::call_super" "${result[4]}"
  assert_equals "tstcls::ClassA0::call_super" "${result[5]}"
  assert_equals 6 "${#result[@]}"
}

function test_ClassB2_call_super_self() {
  local -a result
  mapfile -t result < <( call tstcls::ClassB2.call_super_self )
  assert_equals "tstcls::ClassB2::call_super_self" "${result[0]}"
  assert_equals "tstcls::ClassB1::call_super_self" "${result[1]}"
  assert_equals "tstcls::ClassB0::call_super_self" "${result[2]}"
  assert_equals "tstcls::ClassA2::call_super_self" "${result[3]}"
  assert_equals "tstcls::ClassA1::call_super_self" "${result[4]}"
  assert_equals "tstcls::ClassA0::call_super_self" "${result[5]}"
  assert_equals "tstcls::ClassA2::call_self" "${result[6]}"
  assert_equals 7 "${#result[@]}"
}

#!/usr/bin/env bash

# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck source=../lib/libglb.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libglb.sh"

#-----------------------------------------------------------------------------
#                       libtst helper functions
#-----------------------------------------------------------------------------
declare -ga vartst_array_asc_lst=('one' 'two' 'three' 'four' 'five')
declare -ga vartst_array_dsc_lst=('five' 'four' 'three' 'two' 'one')

#-----------------------------------------------------------------------------
#                           Tests cases 
#-----------------------------------------------------------------------------
function todo_array_colorize() {
  local -a array_lst=("${vartst_array_asc_lst[@]}")

  # Empty color
  array_txt=$(libglb::Array::colorize '' "${array_lst[@]}")
  echo -e "${array_txt}"

  # Cyan color
  array_txt=$(libglb::Array::colorize '\e[96m' "${array_lst[@]}")
  echo -e "${array_txt}"

  # No color
  array_txt=$(NO_COLOR='y' libglb::Array::colorize '\e[96m' "${array_lst[@]}")
  echo -e "${array_txt}"

  # Empty array
  array_lst=()

  # Empty color
  array_txt=$(libglb::Array::colorize '' "${array_lst[@]}")
  [[ "${array_txt}" == '' ]] || return ${?}

  # Cyan color
  array_txt=$(libglb::Array::colorize '\e[96m' "${array_lst[@]}")
  [[ "${array_txt}" == '' ]] || return ${?}

  # No color
  array_txt=$(NO_COLOR='y' libglb::Array::colorize '\e[96m' "${array_lst[@]}")
  [[ "${array_txt}" == '' ]] || return ${?}
  return 0
}

#-----------------------------------------------------------------------------
#                libglb::Array::index functions
#-----------------------------------------------------------------------------
function _tests_array_index_ascend() {
  assert_equals "0" "$(libglb::Array::index 'one'   "${@}")"
  assert_equals "1" "$(libglb::Array::index 'two'   "${@}")"
  assert_equals "2" "$(libglb::Array::index 'three' "${@}")"
  assert_equals "3" "$(libglb::Array::index 'four'  "${@}")"
  assert_equals "4" "$(libglb::Array::index 'five'  "${@}")"
  assert "test -z $(libglb::Array::index 'null' "${@}")"
}

function test_array_index_ascend() {
  local -a array_lst
  local -i array_len=5

  array_lst=("${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  _tests_array_index_ascend     "${array_lst[@]}"
}

function _tests_array_index_descend() {
  assert_equals "0" "$(libglb::Array::index 'five'  "${@}")"
  assert_equals "1" "$(libglb::Array::index 'four'  "${@}")"
  assert_equals "2" "$(libglb::Array::index 'three' "${@}")"
  assert_equals "3" "$(libglb::Array::index 'two'   "${@}")"
  assert_equals "4" "$(libglb::Array::index 'one'   "${@}")"
  assert "test -z $(libglb::Array::index 'null' "${@}")"
}

function test_array_index_descend() {
  local -a array_lst
  local -i array_len=5

  array_lst=("${vartst_array_dsc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  _tests_array_index_descend    "${array_lst[@]}"
}

#-----------------------------------------------------------------------------
#                libglb::Array::join functions
#-----------------------------------------------------------------------------
function test_array_join() {
  local    array_txt array_chk
  local -a array_lst=()
  local -a array_sep_lst=('' ' ' '-' ':' '"' '\n')

  for s in "${array_sep_lst[@]}"; do

    array_chk="one${s}two${s}three"

    array_lst=('one' 'two' 'three')
    array_txt="$(libglb::Array::join "${s}" "${array_lst[@]}")"
    assert_equals "${array_chk}" "${array_txt}"

    array_lst=('' 'one' '' 'two' '' 'three' '')
    array_txt="$(libglb::Array::join "${s}" "${array_lst[@]}")"
    assert_equals "${array_chk}" "${array_txt}"

    array_lst=(' ' 'one' ' ' 'two' ' ' 'three' ' ')
    array_txt="$(libglb::Array::join "${s}" "${array_lst[@]}")"
    assert_equals "${array_chk}" "${array_txt}"

    array_chk=" one ${s} two ${s} three "

    array_lst=(' one ' ' two ' ' three ')
    array_txt="$(libglb::Array::join "${s}" "${array_lst[@]}")"
    assert_equals "${array_chk}" "${array_txt}"

    array_lst=('' ' one ' '' ' two ' '' ' three ')
    array_txt="$(libglb::Array::join "${s}" "${array_lst[@]}")"
    assert_equals "${array_chk}" "${array_txt}"

    array_lst=(' ' ' one ' ' ' ' two ' ' ' ' three ' ' ')
    array_txt="$(libglb::Array::join "${s}" "${array_lst[@]}")"
    assert_equals "${array_chk}" "${array_txt}"
  done
}

#-----------------------------------------------------------------------------
#                libglb::Array::slice_byindex functions
#-----------------------------------------------------------------------------
function test_array_slice_byindex_first() {
  local -a array_lst
  local -i array_len=1

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    '0:0' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  assert_equals 'one'           "${array_lst[0]}"

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    "-${#vartst_array_asc_lst[@]}:-${#vartst_array_asc_lst[@]}" \
    "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  assert_equals 'one'           "${array_lst[0]}"
}

function test_array_slice_byindex_last() {
  local -a array_lst
  local -i array_len=1

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    '4:4' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  assert_equals 'five'          "${array_lst[0]}"

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    '-1:-1' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  assert_equals 'five'          "${array_lst[0]}"
}

function test_array_slice_byindex_middle() {
  local -a array_lst
  local -i array_len=3

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    '1:3' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  assert_equals 'two'           "${array_lst[0]}"
  assert_equals 'three'         "${array_lst[1]}"
  assert_equals 'four'          "${array_lst[2]}"

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    '3:1' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  assert_equals 'four'          "${array_lst[0]}"
  assert_equals 'three'         "${array_lst[1]}"
  assert_equals 'two'           "${array_lst[2]}"
}

function test_array_slice_byindex_whole() {
  local -a array_lst
  local -i array_len=5

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    "0:$(( ${#vartst_array_asc_lst[@]} -1 ))" \
    "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  _tests_array_index_ascend     "${array_lst[@]}"

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    '0:-1' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  _tests_array_index_ascend     "${array_lst[@]}"

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    "$(( ${#vartst_array_asc_lst[@]} -1 )):0" \
    "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  _tests_array_index_descend    "${array_lst[@]}"

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    '-1:0' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  _tests_array_index_descend    "${array_lst[@]}"
}

function test_array_slice_byindex_empty() {
  local -a array_lst
  local -i array_len=0

  mapfile -t array_lst < <(libglb::Array::slice_byindex 'one:five')
  assert_equals "${array_len}" "${#array_lst[@]}"

  mapfile -t array_lst < <(libglb::Array::slice_byindex 'five:one')
  assert_equals "${array_len}" "${#array_lst[@]}"
}

function test_array_slice_byindex_invalid() {
  local -a array_lst
  local -i array_len=0

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    ':' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}" "${#array_lst[@]}"

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    'null:-1' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}" "${#array_lst[@]}"

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    '-1:null' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}" "${#array_lst[@]}"

  mapfile -t array_lst < <(libglb::Array::slice_byindex \
    'null:null' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}" "${#array_lst[@]}"
}

#-----------------------------------------------------------------------------
#                libglb::Array::slice_byvalue functions
#-----------------------------------------------------------------------------
function test_array_slice_byvalue_first() {
  local -a array_lst
  local -i array_len=1

  mapfile -t array_lst < <(libglb::Array::slice_byvalue \
    'one:one' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  assert_equals 'one'           "${array_lst[0]}"
}

function test_array_slice_byvalue_last() {
  local -a array_lst
  local -i array_len=1

  mapfile -t array_lst < <(libglb::Array::slice_byvalue \
    'five:five' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  assert_equals 'five'          "${array_lst[0]}"
}

function test_array_slice_byvalue_middle() {
  local -a array_lst
  local -i array_len=3

  mapfile -t array_lst < <(libglb::Array::slice_byvalue \
    'two:four' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  assert_equals 'two'           "${array_lst[0]}"
  assert_equals 'three'         "${array_lst[1]}"
  assert_equals 'four'          "${array_lst[2]}"

  mapfile -t array_lst < <(libglb::Array::slice_byvalue \
    'four:two' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  assert_equals 'four'          "${array_lst[0]}"
  assert_equals 'three'         "${array_lst[1]}"
  assert_equals 'two'           "${array_lst[2]}"
}

function test_array_slice_byvalue_whole() {
  local -a array_lst
  local -i array_len=5

  mapfile -t array_lst < <(libglb::Array::slice_byvalue \
    'one:five' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  _tests_array_index_ascend     "${array_lst[@]}"

  mapfile -t array_lst < <(libglb::Array::slice_byvalue \
    'five:one' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  _tests_array_index_descend    "${array_lst[@]}"
}

function test_array_slice_byvalue_empty() {
  local -a array_lst
  local -i array_len=0

  mapfile -t array_lst < <(libglb::Array::slice_byvalue 'one:five')
  assert_equals "${array_len}" "${#array_lst[@]}"

  mapfile -t array_lst < <(libglb::Array::slice_byvalue 'five:one')
  assert_equals "${array_len}" "${#array_lst[@]}"
}

function test_array_slice_byvalue_invalid() {
  local -a array_lst
  local -i array_len=0

  # Slice with invalid values
  mapfile -t array_lst < <(libglb::Array::slice_byvalue \
    ':' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}" "${#array_lst[@]}"

  mapfile -t array_lst < <(libglb::Array::slice_byvalue \
    'null:one' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}" "${#array_lst[@]}"

  mapfile -t array_lst < <(libglb::Array::slice_byvalue \
    'one:null' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}" "${#array_lst[@]}"

  mapfile -t array_lst < <(libglb::Array::slice_byvalue \
    'null:null' "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}" "${#array_lst[@]}"
}

#-----------------------------------------------------------------------------
#                libglb::Array::slice_reverse functions
#-----------------------------------------------------------------------------
function test_array_slice_reverse_ascend() {
  local -a array_lst
  local -i array_len=5

  mapfile -t array_lst < <(\
    libglb::Array::reverse "${vartst_array_asc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  _tests_array_index_descend    "${array_lst[@]}"
}

function test_array_slice_reverse_descend() {
  local -a array_lst
  local -i array_len=5

  mapfile -t array_lst < <(\
    libglb::Array::reverse "${vartst_array_dsc_lst[@]}")
  assert_equals "${array_len}"  "${#array_lst[@]}"
  _tests_array_index_ascend     "${array_lst[@]}"
}

function test_array_slice_reverse_empty() {
  local -a array_lst
  local -i array_len=0

  mapfile -t array_lst < <( libglb::Array::reverse )
  assert_equals "${array_len}" "${#array_lst[@]}"
}


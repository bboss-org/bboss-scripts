#!/usr/bin/env bash
# shellcheck disable=SC2016
# SC2016: Expressions don't expand in single quotes.

# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck source=../lib/libglb.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libglb.sh"

#+----------------------------------------------------------------------------
#                           Tests cases 
#-----------------------------------------------------------------------------
function test_variable_dump() {
  local -a array_lst=()
  # shellcheck disable=SC2086
  unset ${!TEST_VAR_*}

  # test_variable_dump must return a valid array
  export TEST_VAR_01='VAR_01'
  export TEST_VAR_02='VAR_02 with space'
#  export TEST_VAR_03='VAR_02 \n with LF'
  mapfile -t array_lst < <(libglb::Environment::dump 'TEST_VAR_*')
  assert_equals "2"   "${#array_lst[@]}"
}

function test_variable_expand() {
  # shellcheck disable=SC2086
  unset ${!TEST_VAR_*}

  # libglb::Environment::expand works in alphabetical order
  export TEST_VAR_01='${PWD}'
  export TEST_VAR_02='${TEST_VAR_01}/varexp'
  assert_equals '${PWD}' "${TEST_VAR_01}"
  assert_equals '${TEST_VAR_01}/varexp' "${TEST_VAR_02}"
  libglb::Environment::expand 'TEST_VAR_*'
  assert_equals "${PWD}" "${TEST_VAR_01}"
  assert_equals "${PWD}/varexp" "${TEST_VAR_02}"

  # Unknow variable is replaced by an empty string
  unset TEST_VAR_01
  export TEST_VAR_02='${TEST_VAR_01}/varexp'
  assert_equals ''                      "${TEST_VAR_01}"
  assert_equals '${TEST_VAR_01}/varexp' "${TEST_VAR_02}"
  libglb::Environment::expand 'TEST_VAR_*'
  assert_equals ''        "${TEST_VAR_01}"
  assert_equals '/varexp' "${TEST_VAR_02}"

  # libglb::Environment::expand is not recursive
  export TEST_VAR_01='${TEST_VAR_02}/varexp'
  export TEST_VAR_02='${PWD}'
  assert_equals '${TEST_VAR_02}/varexp' "${TEST_VAR_01}"
  assert_equals '${PWD}'                "${TEST_VAR_02}"
  libglb::Environment::expand 'TEST_VAR_*'
  assert_equals '${PWD}/varexp'         "${TEST_VAR_01}"
  assert_equals "${PWD}"                "${TEST_VAR_02}"

  # libglb::Environment::expand support content with space
  export TEST_VAR_01='$(uname -v)'
  export TEST_VAR_02='$(uname -r) ${TEST_VAR_01}'
  assert_equals '$(uname -v)'                 "${TEST_VAR_01}"
  assert_equals '$(uname -r) ${TEST_VAR_01}'  "${TEST_VAR_02}"
  libglb::Environment::expand 'TEST_VAR_*'
  assert_equals "$(uname -v)"   "${TEST_VAR_01}"
  assert_equals "$(uname -rv)"  "${TEST_VAR_02}"
}


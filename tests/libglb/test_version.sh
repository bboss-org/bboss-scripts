#!/usr/bin/env bash

# BBOSS Copyright (C) 2018, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck source=../lib/libglb.sh
source "$(dirname "${BASH_SOURCE[0]}")/../lib/libglb.sh"

#-----------------------------------------------------------------------------
#                      libglb::Version::app functions
#-----------------------------------------------------------------------------
function test_version_app_git() {
  local ver_app ver_val ver_chk
  ver_app='git'
  ver_val=$(libglb::Version::app "${ver_app}")
  ver_val=$(printf "${ver_app} version %s" "${ver_val}")
  ver_chk=$("${ver_app}" --version)
  assert_equals "${ver_chk}" "${ver_val}"
}

function test_version_app_snap() {
  local ver_app ver_val ver_chk
  ver_app='snap'
  ver_val=$(libglb::Version::app "${ver_app}")
  ver_val=$(printf "${ver_app}    %s" "${ver_val}")
  ver_chk=$(${ver_app} --version | head -n 1)
  assert_equals "${ver_chk}" "${ver_val}"
}

# Snap package
function test_version_app_shellcheck() {
  local ver_cmd ver_app ver_val ver_chk
  ver_app='shellcheck'
  for ver_cmd in "snap list" "snap list ${ver_app}"; do
    ver_val=$(libglb::Version::app "${ver_cmd}" "${ver_app}")
    ver_val=$(printf "${ver_app}  v%s" "${ver_val}")
    ver_chk=$(snap list "${ver_app}" | tail -n 1)
    assert_equals "${ver_val}" "${ver_chk:0:${#ver_val}}"
  done
}

function test_version_app_python() {
  local ver_cmd ver_app ver_val ver_chk
  ver_cmd='python3'
  ver_app='python'
  ver_val=$(libglb::Version::app "${ver_cmd}" "${ver_app}")
  ver_val=$(printf "${ver_app^} %s" "${ver_val}")
  ver_chk=$("${ver_cmd}" --version)
  assert_equals "${ver_chk}" "${ver_val}"
}

# pip package
function test_version_app_pip() {
  local ver_cmd ver_app ver_val ver_chk
  if command pip3 > /dev/null 2>&1; then
    ver_cmd='pip3 list'
    ver_app='pip'
  else
    ver_cmd='pip list'
    ver_app='pip'
  fi
  ver_val=$(libglb::Version::app "${ver_cmd}" "${ver_app}")
  ver_val=$(printf "${ver_app}==%s" "${ver_val}")
  ver_chk=$(${ver_cmd} --format=freeze 2>/dev/null | grep 'pip==')
  assert_equals "${ver_chk}" "${ver_val}"
}

#-----------------------------------------------------------------------------
#                  libglb::Version::* compare functions
#-----------------------------------------------------------------------------
function _test_version_eq() {
  assert_status_code "0" "libglb::Version::eq ${1} ${2}"
  assert_fail "libglb::Version::lt   ${1} ${2}"
  assert      "libglb::Version::le   ${1} ${2}"
  assert      "libglb::Version::ge   ${1} ${2}"
  assert_fail "libglb::Version::gt   ${1} ${2}"
}
function test_version_eq_01() { _test_version_eq "1"        "1.0.0"; }
function test_version_eq_02() { _test_version_eq "1.0"      "1.0.0"; }
function test_version_eq_03() { _test_version_eq "1.0.0"    "1.0.0"; }
function test_version_eq_04() { _test_version_eq "01"       "1.0.0"; }
function test_version_eq_05() { _test_version_eq "01.02"    "1.2.0"; }
function test_version_eq_06() { _test_version_eq "01.02.03" "1.2.3"; }
function test_version_eq_07() { _test_version_eq; }

function _test_version_lt() {
  assert_status_code "1" "libglb::Version::eq ${1} ${2}"
  assert      "libglb::Version::lt   ${1} ${2}"
  assert      "libglb::Version::le   ${1} ${2}"
  assert_fail "libglb::Version::ge   ${1} ${2}"
  assert_fail "libglb::Version::gt   ${1} ${2}"
}
function test_version_lt_01() { _test_version_lt "1"      "1.0.1"; }
function test_version_lt_02() { _test_version_lt "1.0"    "1.0.1"; }
function test_version_lt_03() { _test_version_lt "1.0.0"  "1.0.1"; }

function _test_version_gt() {
  assert_status_code "2" "libglb::Version::eq ${1} ${2}"
  assert_fail "libglb::Version::lt   ${1} ${2}"
  assert_fail "libglb::Version::le   ${1} ${2}"
  assert      "libglb::Version::ge   ${1} ${2}"
  assert      "libglb::Version::gt   ${1} ${2}"
}
function test_version_gt_01() { _test_version_gt "1.0.1"  "1";     }
function test_version_gt_02() { _test_version_gt "1.0.1"  "1.0";   }
function test_version_gt_03() { _test_version_gt "1.0.1"  "1.0.0"; }
function test_version_gt_04() { _test_version_gt "1"; }

#-----------------------------------------------------------------------------
#                      libglb::Version::os functions
#-----------------------------------------------------------------------------
function test_version_os_release() {
  local ver_val ver_chk
  # Distributor ID: Ubuntu
  ver_val="$(libglb::Version::os_release 'ID')"
  ver_chk="$(lsb_release -is)"
  assert_equals "${ver_val}" "${ver_chk,,}"
  # Description:  Ubuntu 18.04.3 LTS
  ver_val="$(libglb::Version::os_release 'PRETTY_NAME')"
  ver_chk="$(lsb_release -ds)"
  assert_equals "${ver_chk}" "${ver_val}"
  # Release:  18.04
  ver_val="$(libglb::Version::os_release 'VERSION_ID')"
  ver_chk="$(lsb_release -rs)"
  assert_equals "${ver_chk}" "${ver_val}"
  # Codename: bionic
  ver_val="$(libglb::Version::os_release 'VERSION_CODENAME')"
  ver_chk="$(lsb_release -cs)"
  assert_equals "${ver_chk}" "${ver_val}"
  # Empty
  ver_val="$(libglb::Version::os_release '')"
  assert_equals "${ver_val}" ""
  # Unknown
  ver_val="$(libglb::Version::os_release 'UNKNOWN')"
  assert_equals "${ver_val}" ""
}

function test_version_os_distrib() {
  local ver_val ver_chk
  ver_val="$(libglb::Version::os_distrib)"
  ver_chk="$(lsb_release -is)"
  assert_equals "${ver_val}" "${ver_chk,,}"
}

function test_version_os_version() {
  local ver_val ver_chk
  ver_val="$(libglb::Version::os_version)"
  ver_chk="$(lsb_release -rs)"
  assert_equals "${ver_chk}" "${ver_val}"
}


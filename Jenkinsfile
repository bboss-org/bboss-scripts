
// BBOSS Copyright (C) 2018, Laurent Marchelli
// SPDX-License-Identifier: GPL-3.0-or-later

//
//  Execute the same command with one or several nodes in parallel.
//
//  node_filter : Node filter used to list all available nodes
//  arch_name   : Node architecture (adm64|armhf|etc..)
//
def build_parallelized(nde_filter, nde_cmd, arch_map) {
  def stg_stp = ("${STAGE_NAME}" =~ '^([0-9]*)\\..*')[0][1]
  def stg_res = 'SUCCESS'
  def nde_map = [:]
  def nde_cnt = 1

  arch_map.each { arch_name, arch_count ->
    def nde_lbl = (nde_filter ? "${nde_filter} && " : "") + arch_name
    for(int i=1; i <= arch_count; i++) {
      def nde_fmt = (arch_map.size() > 1 || arch_count > 1) ? 
        String.format("${stg_stp}~%02d", nde_cnt) : "${stg_stp}"
      def nde_num  = String.format("${nde_fmt}", nde_cnt)
      def nde_env = [
        'BUILD_SCT_LOGPATH=${WORKSPACE}/output/' + "logs_${nde_num}/",
        'BUILD_SCT_OUTPATH=${WORKSPACE}/output/',
        "BUILD_SCT_RUNINDEX=${i.toString()}",
        "BUILD_SCT_RUNTOTAL=${arch_count.toString()}",
        'BUILD_SCT_RUNURL=${BUILD_URL}artifact/output/' + "logs_${nde_num}/",
      ]
      nde_map[nde_num] = {
        // Steps batch to run in parallel on one or several nodes for the
        // specified architecture
        node(nde_lbl) {
          def nde_scm = checkout(scm)
          // Set node environment
          withEnv(nde_env) {
            // Use catchError instead than try / catch to get a red line when
            // the step fail or is aborted.
            catchError(buildResult: 'FAILURE', stageResult: 'FAILURE')
            { 
              sh "set +x && ${nde_cmd}" 
            }
            // Upload build logs
            archiveArtifacts(
              allowEmptyArchive: true,
              artifacts: "output/logs_${nde_num}/*"
            )
            if(currentBuild.result == 'FAILURE')
              { stg_res = currentBuild.result }
          }
        }
      } 
      nde_cnt ++
    }
  }
  parallel(nde_map)
  // Throw (forward) build error to stop the pipeline.
  if(stg_res == 'FAILURE') { error }
}

// Declarative Pipeline default parameters
def bld_arch = 'amd64'
def bld_params =[
  'BUILD_SCT_PRODUCT'  : ['bboss-script'],
]

pipeline {
  parameters {
    choice(name: 'BUILD_SCT_PRODUCT',
      choices: bld_params['BUILD_SCT_PRODUCT'], 
      description: 'Product name')
  }
  options {
    ansiColor('xterm')
    disableConcurrentBuilds()
    skipDefaultCheckout(true)
    buildDiscarder logRotator(
      artifactDaysToKeepStr: '', 
      artifactNumToKeepStr: '', 
      daysToKeepStr: '', 
      numToKeepStr: '10'
    )
  }
  environment {
    // ansiColor bug fix : 
    // https://issues.jenkins-ci.org/browse/JENKINS-55139?src=confmacro
    NO_COLOR='y'
    BUILD_SCT_GITBRANCH=String.format(env.BRANCH_NAME ?: '')
  }
  agent none
  stages {
    stage('01. Test Install') {
      steps {
        build_parallelized("",
          './build.sh -c install:install -b install:install',
          ["${bld_arch}": 6]
        )
      }
    }
    stage('02. Deploy') {
      steps {
        build_parallelized("",
          './build.sh -c venv:deploy -b venv:deploy', 
          ["${bld_arch}": 1]
        )
      }
    }
  }
}

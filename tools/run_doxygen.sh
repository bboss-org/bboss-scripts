#!/usr/bin/env bash

# BBOSS Copyright (C) 2019, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later
# https://gitlab.com/bboss-org/bboss-scripts

export BUILD_SCT_PRODUCT="bboss-scripts"
export BUILD_SCT_PRODDES="Script library simplifying CI/CD steps implementation and parallelization."
export BUILD_SCT_PRODVER="1.0"
export BUILD_SCT_OUTPATH="output/documentation"

mkdir -p "${BUILD_SCT_OUTPATH}"
doxygen
exit
sed -n -f ./tools/doxygen-bash.sed ./lib/libsct.sh | sed 'N;/^\n$/D;P;D' > ./lib/sedout.txt
sed -n -f ./tools/doxygen-bash.sed ./lib/libsct.sh | cat -s > ./lib/sedout.txt

sed -n -f ./tools/doxygen-bash.sed ./lib/libobj.sh | sed 'N;/^\n$/D;P;D' > ./lib/sedout.txt
sed -n -f ./tools/doxygen-bash.sed ./lib/libobj.sh | cat -s > ./lib/sedout.txt